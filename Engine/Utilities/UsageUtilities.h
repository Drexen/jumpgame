#pragma once

#include <Psapi.h>
#include <Windows.h>

#include <Engine\Utilities\ErrorOutput.h>

unsigned int GetApplicationMemoryUsageMB()
{
	PROCESS_MEMORY_COUNTERS memoryCounter;

	if (!GetProcessMemoryInfo(GetCurrentProcess(), &memoryCounter, sizeof(PROCESS_MEMORY_COUNTERS)))
	{
		ErrorOutput::Print("Error: Usage utilities.", "Function K32GetProcessMemoryInfo failed.");
	}

	unsigned int memorySize = (unsigned int)memoryCounter.WorkingSetSize;
	unsigned int memorySizeMB = memorySize / 1024 / 1024;

	return memorySizeMB;
}
