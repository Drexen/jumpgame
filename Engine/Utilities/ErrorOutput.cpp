#include "ErrorOutput.h"

bool ErrorOutput::mInitialised = false;
HWND ErrorOutput::mWindow = nullptr;

void ErrorOutput::Initialise(HWND window)
{
	if (!mInitialised)
	{
		mWindow = window;
		mInitialised = true;
	}
}

void ErrorOutput::Print(std::string title, std::string message)
{
	if (mInitialised)
	{
		MessageBox(
			mWindow,
			std::wstring(message.begin(), message.end()).c_str(),
			std::wstring(title.begin(), title.end()).c_str(),
			MB_OK);
	}
}
