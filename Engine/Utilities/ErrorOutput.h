#pragma once

#include <Windows.h>
#include <string>

class ErrorOutput
{
public:

	static void Initialise(HWND window);

	static void Print(std::string title, std::string message);

private:

	static bool mInitialised;
	static HWND mWindow;
};
