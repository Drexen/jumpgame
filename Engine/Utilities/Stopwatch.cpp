#include "Stopwatch.h"

void Stopwatch::Start()
{
	mStartTime = std::chrono::high_resolution_clock::now();
}

double Stopwatch::GetTimeSinceStart()
{
	std::chrono::high_resolution_clock::time_point currentTime =
		std::chrono::high_resolution_clock::now();
	
	std::chrono::duration<double> difference =
		std::chrono::duration_cast<std::chrono::duration<double>>(currentTime - mStartTime);

	return difference.count();
}
