#pragma once

#include <chrono>

class Stopwatch
{
public:
	Stopwatch() = default;

	void Start();

	// time in seconds since start call
	double GetTimeSinceStart();

private:

	std::chrono::high_resolution_clock::time_point mStartTime;
};
