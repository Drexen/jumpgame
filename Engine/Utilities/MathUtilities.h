#pragma once

#include <DirectXMath.h>

inline bool CompareFloat3(DirectX::XMFLOAT3& a, DirectX::XMFLOAT3& b)
{
	if (a.x == b.x &&
		a.y == b.y &&
		a.z == b.z)
	{
		return true;
	}
	else
	{
		return false;
	}
}

inline DirectX::XMVECTOR Float3ToVector(DirectX::XMFLOAT3& in)
{
	return XMLoadFloat3(&in);
}

inline DirectX::XMFLOAT3 VectorToFloat3(DirectX::XMVECTOR& in)
{
	DirectX::XMFLOAT3 out;
	XMStoreFloat3(&out, in);
	return out;
}

inline DirectX::XMFLOAT3 AddFloat3(DirectX::XMFLOAT3 a, DirectX::XMFLOAT3 b)
{
	DirectX::XMFLOAT3 out;
	
	out = VectorToFloat3(DirectX::XMVectorAdd(Float3ToVector(a), Float3ToVector(b)));
	return out;
}

inline DirectX::XMFLOAT3 SubtractFloat3(DirectX::XMFLOAT3 a, DirectX::XMFLOAT3 b)
{
	DirectX::XMFLOAT3 out;
	out = VectorToFloat3(DirectX::XMVectorSubtract(Float3ToVector(a), Float3ToVector(b)));
	return out;
}

inline DirectX::XMFLOAT3 MultiplyFloat3(DirectX::XMFLOAT3 a, float b)
{
	DirectX::XMFLOAT3 out;
	out = { a.x * b, a.y * b, a.z * b };
	return out;
}

inline DirectX::XMFLOAT3 MultiplyFloat3(DirectX::XMFLOAT3 a, DirectX::XMFLOAT3 b)
{
	DirectX::XMFLOAT3 out;
	out = { a.x * b.x, a.y * b.y, a.z * b.z };
	return out;
}





inline DirectX::XMVECTOR SInt2ToVector(DirectX::XMINT2& in)
{
	return XMLoadSInt2(&in);
}

inline DirectX::XMINT2 VectorToSInt2(DirectX::XMVECTOR& in)
{
	DirectX::XMINT2 out;
	XMStoreSInt2(&out, in);
	return out;
}

inline DirectX::XMINT2 AddSInt2(DirectX::XMINT2 a, DirectX::XMINT2 b)
{
	DirectX::XMINT2 out;
	out = VectorToSInt2(DirectX::XMVectorAdd(SInt2ToVector(a), SInt2ToVector(b)));
	return out;
}

inline DirectX::XMINT2 SubtractSInt2(DirectX::XMINT2 a, DirectX::XMINT2 b)
{
	DirectX::XMINT2 out;
	out = VectorToSInt2(DirectX::XMVectorSubtract(SInt2ToVector(a), SInt2ToVector(b)));
	return out;
}

constexpr float ERROR_AMOUNT = 0.0001f;

inline float RoundFloatError(float in)
{
	float upper = ceilf(in);
	if ((upper - in) < ERROR_AMOUNT)
	{
		return upper;
	}

	float lower = floorf(in);
	if ((in - lower) < ERROR_AMOUNT)
	{
		return lower;
	}

	return in;
}

inline void RoundFloat3Error(DirectX::XMFLOAT3& in)
{
	in.x = RoundFloatError(in.x);
	in.y = RoundFloatError(in.y);
	in.z = RoundFloatError(in.z);
}
