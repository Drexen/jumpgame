#pragma once

#include <random>
#include <chrono>

class RandomGenerator
{
public:

	static void Initialise();
	static int GetRandomInt(int min, int max);
	static float GetRandomFloat(float min, float max);

private:

	static std::mt19937 random;
};
