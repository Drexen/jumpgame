#include "RandomGenerator.h"

std::mt19937 RandomGenerator::random;

void RandomGenerator::Initialise()
{
	random.seed((unsigned int)std::chrono::high_resolution_clock::now().time_since_epoch().count());
}

int RandomGenerator::GetRandomInt(int min, int max)
{
	std::uniform_int_distribution<int> distribution(min, max);

	return distribution(random);
}

float RandomGenerator::GetRandomFloat(float min, float max)
{
	std::uniform_real_distribution<float> distribution(min, max);

	return distribution(random);
}
