#include "Mesh.h"

#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\Graphics\Textures\TextureManager.h>
#include <Engine\Graphics\Models\Model.h>
#include <Engine\Utilities\ErrorOutput.h>
#include <Engine\Graphics\Models\ModelManager.h>

Mesh::Mesh() :
	mInitialised(false)
{

}

void Mesh::Initialise(ID3D11Device* device, MeshData data)
{
	if (mInitialised)
	{
		return;
	}

	mVertexBuffer = 0;
	mInstanceBuffer = 0;
	mTexture = 0;

	UpdateMeshData(device, data);

	mInitialised = true;
}

void Mesh::Shutdown()
{
	// release the instance buffer
	if (mInstanceBuffer)
	{
		mInstanceBuffer->Release();
		mInstanceBuffer = 0;
	}

	// release the vertex buffer
	if (mVertexBuffer)
	{
		mVertexBuffer->Release();
		mVertexBuffer = 0;
	}
}

void Mesh::SetupForRender(ID3D11DeviceContext* deviceContext)
{
	unsigned int strides[2];
	unsigned int offsets[2];
	ID3D11Buffer* bufferPointers[2];

	// set buffer strides
	strides[0] = sizeof(VertexType);
	strides[1] = sizeof(InstanceType);

	// set the buffer offsets
	offsets[0] = 0;
	offsets[1] = 0;

	// set the array of pointers to the vertex and instance buffers
	bufferPointers[0] = mVertexBuffer;
	bufferPointers[1] = mInstanceBuffer;

	// set the vertex and instance buffers to active in the input assembler so it can be rendered
	deviceContext->IASetVertexBuffers(0, 2, bufferPointers, strides, offsets);

	// set the type of primitive that should be rendered from this vertex buffer
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Mesh::UpdateMeshData(ID3D11Device* device, MeshData data)
{
	Model* model;
	std::vector<Mesh::VertexType> vertices;
	std::vector<Mesh::InstanceType> instances;
	bool result;

	// get vertex data
	if (!data.modelName.empty())
	{
		model = ModelManager::GetModel(data.modelName);

		// use model vertex data
		vertices = model->mVertices;
	}
	else if (!data.vertexData.empty())
	{
		// use vertex data for model verts
		vertices = data.vertexData;
	}
	else
	{
		ErrorOutput::Print("Error: Mesh", "No vertex data available.");
	}

	// get position data
	if (!data.instanceData.empty())
	{
		instances = data.instanceData;
	}
	else if (!(data.pos.x == FLT_MIN))
	{
		Mesh::InstanceType instType;
		instType.mPosition = data.pos;
		instances.push_back(instType);
	}
	else
	{
		ErrorOutput::Print("Error: Mesh", "No position data available.");

	}

	// get texture data
	if (data.overrideTexture != nullptr)
	{
		mTexture = data.overrideTexture;
	}
	else if (!data.overrideTextureName.empty())
	{
		mTexture = TextureManager::GetTextureFromPNG(data.overrideTextureName);
	}
	else if (model != nullptr)
	{
		mTexture = TextureManager::GetTextureFromPNG(data.modelName + ".png");
	}
	else
	{
		ErrorOutput::Print("Error: Mesh", "No texture data available.");
	}

	// initialise mesh buffers
	result = InitialiseBuffers(device, vertices, instances);
	if (!result)
	{
		ErrorOutput::Print("Error: Mesh", "Could not initialise the mesh object buffers.");
	}
}

bool Mesh::InitialiseBuffers(ID3D11Device* device, std::vector<VertexType> vertices, std::vector<InstanceType> vInstances)
{
	// validate the vertices and indices vectors
	if (vertices.size() == 0)
	{
		return false;
	}

	D3D11_BUFFER_DESC vertexBufferDesc; // indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData; // indexData;
	HRESULT hr;

	// initialise the vertex and index count
	mVertexCount = (int)vertices.size();

	// set up the description of the static vertex buffer
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// give the subresource structure a pointer to the vertex data
	vertexData.pSysMem = &vertices[0];
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// create the vertex buffer
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// set the number of instances in the array
	mInstanceCount = (int)vInstances.size();

	// initialise the instances array with data from the vector of points
	InstanceType* instances;
	instances = new InstanceType[mInstanceCount];
	instances = &vInstances[0];

	// set up the description of the instance buffer
	D3D11_BUFFER_DESC instanceBufferDesc;
	D3D11_SUBRESOURCE_DATA instanceData;

	instanceBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	instanceBufferDesc.ByteWidth = sizeof(InstanceType) * mInstanceCount;
	instanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instanceBufferDesc.CPUAccessFlags = 0;
	instanceBufferDesc.MiscFlags = 0;

	// give the subresource structure a pointer to the instance data
	instanceData.pSysMem = instances;

	// create the instance buffer
	hr = device->CreateBuffer(&instanceBufferDesc, &instanceData, &mInstanceBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// release the arrays now that the vertex and index buffers have been created and loaded
	vertices.clear();

	return true;
}

std::vector<Mesh::InstanceType> Mesh::PosVectorToInstanceType(std::vector<DirectX::XMFLOAT3> in)
{
	std::vector<Mesh::InstanceType> output;

	for (auto i = in.begin(); i != in.end(); i++)
	{
		Mesh::InstanceType instType;
		instType.mPosition = *i;

		output.push_back(instType);
	}

	return output;
}

std::vector<Mesh::VertexType> Mesh::GenerateVertices2DPlane(UINT width, UINT height, int xOffs, int yOffs)
{
	// screen mesh
	float xmax = ((float)width / 2.0f) - xOffs;
	float xmin = -((float)width / 2.0f) - xOffs;

	float ymax = ((float)height / 2.0f) - yOffs;
	float ymin = -((float)height / 2.0f) - yOffs;

	std::vector<Mesh::VertexType> vertices;
	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmin, ymin, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f))); // bottom right
	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmax, ymin, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f))); // bottom left
	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmax, ymax, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f))); // top left

	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmin, ymin, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f))); // bottom right
	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmax, ymax, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f))); // top left
	vertices.push_back(Mesh::VertexType(DirectX::XMFLOAT3(xmin, ymax, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f))); // top right

	return vertices;
}
