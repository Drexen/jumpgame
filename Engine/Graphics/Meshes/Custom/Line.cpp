#include "Line.h"

#include <Engine\Utilities\MathUtilities.h>

Line::Line(ID3D11Device* device, DirectX::XMFLOAT3 start, DirectX::XMFLOAT3 end)
{
	Mesh::MeshData meshData;
	meshData.pos = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	meshData.modelName = "resource/models/cube";
	meshData.overrideTextureName = "resource/red.png";

	Initialise(device, meshData);

	// initialise the transformMatrix from the start and end positions
	// line length
	DirectX::XMFLOAT3 lineLengthTotal = DirectX::XMFLOAT3();
	lineLengthTotal = SubtractFloat3(start, end);

	float lineLength = sqrt(
		lineLengthTotal.x * lineLengthTotal.x +
		lineLengthTotal.y * lineLengthTotal.y +
		lineLengthTotal.z * lineLengthTotal.z);

	// line position
	DirectX::XMFLOAT3 linePos = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	linePos = AddFloat3(end, MultiplyFloat3(lineLengthTotal, 0.5f));

	// line rotation
	DirectX::XMFLOAT3 forward;
	DirectX::XMFLOAT3 up;
	DirectX::XMFLOAT3 right;

	XMStoreFloat3(&forward, DirectX::XMVector3Normalize(XMLoadFloat3(&lineLengthTotal)));

	if (CompareFloat3(forward, DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f)) ||
		CompareFloat3(forward, DirectX::XMFLOAT3(0.0f, -1.0f, 0.0f)))
	{
		up = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
	}
	else
	{
		up = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	}

	XMStoreFloat3(&right, DirectX::XMVector3Cross(XMLoadFloat3(&forward), XMLoadFloat3(&up)));
	XMStoreFloat3(&right, DirectX::XMVector3Normalize(XMLoadFloat3(&right)));

	DirectX::XMMATRIX rotationMatrix = DirectX::XMMATRIX(
		right.x, right.y, right.z, 0.0f,
		forward.x, forward.y, forward.z, 0.0f,
		up.x, up.y, up.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	// initialise transformMatrix from data created
	DirectX::XMMATRIX finalMatrix = DirectX::XMMatrixIdentity();
	finalMatrix = DirectX::XMMatrixIdentity();
	finalMatrix *= DirectX::XMMatrixScaling(0.05f, lineLength, 0.05f);
	finalMatrix *= rotationMatrix;
	finalMatrix *= DirectX::XMMatrixTranslation(linePos.x, linePos.y, linePos.z);

	XMStoreFloat4x4(&transformMatrix, finalMatrix);
}
