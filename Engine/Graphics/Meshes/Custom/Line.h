#pragma once

#include <Engine\Graphics\Meshes\Mesh.h>

class Line : public Mesh
{
public:

	Line(ID3D11Device* device, DirectX::XMFLOAT3 start, DirectX::XMFLOAT3 end);

private:

	DirectX::XMFLOAT4X4 transformMatrix;
};
