#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <vector>

#include <Engine\Graphics\Shaders\Shader.h>

class D3DManager;
class Model;

class Mesh
{
public:

	struct VertexType
	{
		VertexType() {};

		VertexType(DirectX::XMFLOAT3 position, DirectX::XMFLOAT2 uv) :
			mPosition(position),
			mUV(uv) {}

		DirectX::XMFLOAT3 mPosition;
		DirectX::XMFLOAT2 mUV;
	};

	struct InstanceType
	{
		InstanceType() {};

		InstanceType(DirectX::XMFLOAT3 pos) :
			mPosition(pos)
		{}

		DirectX::XMFLOAT3 mPosition;
	};

	struct MeshData
	{
		MeshData() :
			overrideTexture(nullptr),
			pos(DirectX::XMFLOAT3(FLT_MIN, FLT_MIN, FLT_MIN))
		{
			
		}

		std::string modelName;
		std::vector<Mesh::VertexType> vertexData;
		std::vector<Mesh::InstanceType> instanceData;
		DirectX::XMFLOAT3 pos;
		std::string overrideTextureName;
		ID3D11ShaderResourceView* overrideTexture;

	};

public:
	Mesh();

	virtual void Initialise(ID3D11Device* device, MeshData data);

	inline int GetVertexCount() { return mVertexCount; }
	inline int GetInstanceCount() { return mInstanceCount; }

	inline ID3D11ShaderResourceView* GetTexture() { return mTexture; }

	void UpdateMeshData(ID3D11Device* device, MeshData data);

	void Shutdown();

	static std::vector<Mesh::InstanceType> PosVectorToInstanceType(std::vector<DirectX::XMFLOAT3> in);
	
	void SetupForRender(ID3D11DeviceContext* deviceContext);

	static std::vector<Mesh::VertexType> GenerateVertices2DPlane(UINT width, UINT height, int xOffs, int yOffs);

private:

	bool InitialiseBuffers(
		ID3D11Device* device,
		std::vector<VertexType> vertices,
		std::vector<InstanceType> vInstances);

	ID3D11Buffer* mVertexBuffer;
	ID3D11Buffer* mInstanceBuffer;
	ID3D11ShaderResourceView* mTexture;

	bool mInitialised;

	int mVertexCount;
	int mInstanceCount;
};
