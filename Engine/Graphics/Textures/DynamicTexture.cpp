#include "DynamicTexture.h"

#include <Engine\Utilities\ErrorOutput.h>

DynamicTexture::DynamicTexture() :
	mInitialised(false)
{

}

void DynamicTexture::Initialise(
	ID3D11Device* device,
	ID3D11DeviceContext* deviceContext,
	const UINT width,
	const UINT height)
{
	if (mInitialised)
	{
		return;
	}

	mDevice = device;
	mDeviceContext = deviceContext;
	mWidth = width;
	mHeight = height;


	// MAIN TEXTURE

	// create texture description
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = mWidth;
	textureDesc.Height = mHeight;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	// create subresource data
	unsigned int arraySize = mWidth * mHeight;
	uint32_t* textureData = new uint32_t[arraySize];

	for (unsigned int i = 0; i < arraySize; i++)
	{
		textureData[i] = uint32_t(0xffffffff);
	}

	D3D11_SUBRESOURCE_DATA subresourceData;
	subresourceData.pSysMem = (void*)textureData;
	subresourceData.SysMemPitch = (UINT)(mWidth * sizeof(uint32_t));
	subresourceData.SysMemSlicePitch = 0;

	// create texture	
	HRESULT hr = device->CreateTexture2D(&textureDesc, &subresourceData, &mTexture);
	if (FAILED(hr))
	{
		ErrorOutput::Print("Error: Dynamic texture", "Could not create texture.");
	}

	// create shader resource view
	hr = device->CreateShaderResourceView(mTexture, 0, &mSRV);
	if (FAILED(hr))
	{
		ErrorOutput::Print("Error: Dynamic texture", "Could not create shader resource view from texture.");
	}

	mInitialised = true;
}

void DynamicTexture::Shutdown()
{
	if (mSRV)
	{
		mSRV->Release();
		mSRV = nullptr;
	}

	if (mTexture)
	{
		mTexture->Release();
		mTexture = nullptr;
	}
}

ID3D11ShaderResourceView* DynamicTexture::GetTexture()
{
	if (!mInitialised)
	{
		ErrorOutput::Print("Error: Dynamic texture", "Not initialised.");
		return nullptr;
	}

	return mSRV;
}

void DynamicTexture::UpdateTexture(uint32_t* textureData)
{
	if (!mInitialised)
	{
		ErrorOutput::Print("Error: Dynamic texture", "Not initialised.");
	}

	HRESULT hr;

	// STAGING TEXTURE

	// create texture description
	D3D11_TEXTURE2D_DESC textureDescStaging;
	textureDescStaging.Width = mWidth;
	textureDescStaging.Height = mHeight;
	textureDescStaging.MipLevels = 1;
	textureDescStaging.ArraySize = 1;
	textureDescStaging.Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
	textureDescStaging.SampleDesc.Count = 1;
	textureDescStaging.SampleDesc.Quality = 0;
	textureDescStaging.Usage = D3D11_USAGE_STAGING;
	textureDescStaging.BindFlags = 0;
	textureDescStaging.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
	textureDescStaging.MiscFlags = 0;

	// create subresource data
	D3D11_SUBRESOURCE_DATA subresourceDataStaging;
	subresourceDataStaging.pSysMem = (void*)textureData;
	subresourceDataStaging.SysMemPitch = (UINT)(mWidth * sizeof(uint32_t));
	subresourceDataStaging.SysMemSlicePitch = 0;

	// create staging texture
	ID3D11Texture2D* textureStaging;
	hr = mDevice->CreateTexture2D(&textureDescStaging, &subresourceDataStaging, &textureStaging);
	if (FAILED(hr))
	{
		ErrorOutput::Print("Error: Screen texture manager", "Could not create custom staging texture.");
	}

	// copy textureStaging data over to texture
	mDeviceContext->CopyResource(mTexture, textureStaging);

	// recreate srv with new texture data
	hr = mDevice->CreateShaderResourceView(mTexture, 0, &mSRV);
	if (FAILED(hr))
	{
		ErrorOutput::Print("Error: Screen texture manager", "Could not create shader resource view from texture.");
	}

	// cleanup
	textureStaging->Release();
}
