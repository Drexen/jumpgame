#pragma once

#include <d3d11.h>
#include <Windows.h>
#include <cstdint>
#include <DirectXMath.h>

class RenderTexture
{
public:

	RenderTexture();

	void Initialise(
		ID3D11Device* device,
		ID3D11DeviceContext* deviceContext,
		const UINT width,
		const UINT height);

	void Clear(float red, float green, float blue, float alpha);
	
	void Shutdown();

	ID3D11RenderTargetView* GetRenderTargetView() { return mRenderTargetView; }
	ID3D11DepthStencilView* GetDepthStencilView() { return mDepthStencilView; }
	ID3D11ShaderResourceView* GetTexture() { return mSRV; }

private:

	bool mInitialised;

	ID3D11Device* mDevice;
	ID3D11DeviceContext* mDeviceContext;
	ID3D11Texture2D* mTexture;
	ID3D11ShaderResourceView* mSRV;

	ID3D11RenderTargetView* mRenderTargetView;
	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilState* mDepthStencilState;
	ID3D11DepthStencilView* mDepthStencilView;

	UINT mWidth;
	UINT mHeight;
};
