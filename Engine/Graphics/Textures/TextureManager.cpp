#include "TextureManager.h"

#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\Utilities\ErrorOutput.h>
#include <Engine\Utilities\RandomGenerator.h>

bool TextureManager::mInitialised = false;
ID3D11Device* TextureManager::mDevice = 0;
HWND TextureManager::mWindow = 0;
std::unordered_map<std::string, TextureManager::Texture> TextureManager::mTextures;

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{

}

void TextureManager::Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext, HWND window)
{
	if (mInitialised)
	{
		return;
	}

	mDevice = device;
	mWindow = window;

	mInitialised = true;
}

void TextureManager::Shutdown()
{
	// clear the texture map
	for (auto i = mTextures.begin(); i != mTextures.end();)
	{
		if (i->second.loaded)
		{
			i->second.texture->Release();
		}
		
		i = mTextures.erase(i);
	}

	mInitialised = 0;
}

ID3D11ShaderResourceView* TextureManager::GetTextureFromPNG(std::string filename)
{
	if (mInitialised)
	{
		// check if the texture has been previously loaded
		if (!mTextures[filename].loaded)
		{
			// load the texture
			HRESULT result;

			DirectX::ScratchImage* image = new DirectX::ScratchImage();
			
			result = LoadFromWICFile(std::wstring(filename.begin(), filename.end()).c_str(), DirectX::WIC_FLAGS_NONE, nullptr, *image);
			if (FAILED(result))
			{
				std::string message = "Error loading PNG: " + filename;
				MessageBox(mWindow, std::wstring(message.begin(), message.end()).c_str(), L"Error", MB_OK);
				return nullptr;
			}

			ID3D11Resource* resource;

			result = CreateTexture(mDevice, image->GetImages(), 1, image->GetMetadata(), &resource);
			if (FAILED(result))
			{
				MessageBox(mWindow, L"Error generating texture", L"Error", MB_OK);
				return nullptr;
			}

			ID3D11ShaderResourceView* shaderResourceView;
			result = mDevice->CreateShaderResourceView(resource, 0, &shaderResourceView);
			if (FAILED(result))
			{
				MessageBox(mWindow, L"Error generating shader resource view from resource", L"Error", MB_OK);
				return nullptr;
			}

			mTextures[filename].texture = shaderResourceView;
			mTextures[filename].loaded = true;

			// clean up
			image->Release();
			resource->Release();

			return shaderResourceView;
		}
		else
		{
			// since it has been loaded previously, return a pointer to the texture
			return mTextures[filename].texture;
		}
		
	}
	else
	{
		MessageBox(mWindow, L"Error, TextureManager not initialised to load a texture", L"Error", MB_OK);
		return nullptr;
	}	
}
