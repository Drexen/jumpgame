#pragma once

#include <d3d11.h>
#include <Windows.h>
#include <cstdint>

class DynamicTexture
{
public:

	DynamicTexture();

	void Initialise(
		ID3D11Device* device,
		ID3D11DeviceContext* deviceContext,
		const UINT width,
		const UINT height);

	ID3D11ShaderResourceView* GetTexture();

	void UpdateTexture(uint32_t* textureData);

	void Shutdown();

private:

	bool mInitialised;

	ID3D11Device* mDevice;
	ID3D11DeviceContext* mDeviceContext;
	ID3D11ShaderResourceView* mSRV;	
	ID3D11Texture2D* mTexture;

	UINT mWidth;
	UINT mHeight;
};
