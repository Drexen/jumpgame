#pragma once

#include <d3d11.h>
#include <DirectXTex.h>
#include <unordered_map>
#include <string>

class TextureManager
{
private:

	struct Texture
	{
		Texture() : loaded(false) {}
		ID3D11ShaderResourceView* texture;
		bool loaded;
	};

public:

	static void Initialise(ID3D11Device* device, ID3D11DeviceContext* deviceContext,HWND window);
	static void Shutdown();

	static ID3D11ShaderResourceView* GetTextureFromPNG(std::string filename);

private:

	TextureManager();
	~TextureManager();

	static bool mInitialised;

	static std::unordered_map<std::string, Texture> mTextures;

	static ID3D11Device* mDevice;
	static HWND mWindow;
};
