#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <fstream>
#include <d3dx11effect.h>

class Mesh;

class Shader
{
	friend class Mesh;

public:

	Shader();
	~Shader();

	virtual void Shutdown();

protected:	

	ID3DX11Effect* mEffect;
	ID3DX11EffectTechnique* mTechnique;
	ID3D11InputLayout* mLayout;

	ID3DX11EffectMatrixVariable* mWorldMatrixPtr;
	ID3DX11EffectMatrixVariable* mViewMatrixPtr;
	ID3DX11EffectMatrixVariable* mProjectionMatrixPtr;
	ID3DX11EffectShaderResourceVariable* mTexturePtr;
};
