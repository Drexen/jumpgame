#include "Shader.h"

Shader::Shader()
{
	mEffect = 0;
	mTechnique = 0;
	mLayout = 0;

	mWorldMatrixPtr = 0;
	mViewMatrixPtr = 0;
	mProjectionMatrixPtr = 0;
	mTexturePtr = 0;
}

Shader::~Shader()
{

}

void Shader::Shutdown()
{
	mTexturePtr = 0;
	mWorldMatrixPtr = 0;
	mViewMatrixPtr = 0;
	mProjectionMatrixPtr = 0;
	mTechnique = 0;

	if (mLayout)
	{
		mLayout->Release();
		mLayout = 0;
	}

	if (mEffect)
	{
		mEffect->Release();
		mEffect = 0;
	}
}
