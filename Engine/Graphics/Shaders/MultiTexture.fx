// Globals
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
Texture2D shaderTexture1;
Texture2D shaderTexture2;

// sample states
SamplerState SampleType
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

// type defs
struct VertexInputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 instancePosition : TEXCOORD1;
};

struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
};

// vertex shader
PixelInputType MultiTextureVertexShader(VertexInputType input)
{
	PixelInputType output;
	
	// change the position vector to be 4 units for proper matrix calculations
	input.position.w = 1.0f;
	
	// update the position of the vertices based on the data for this particular instance
	input.position.x += input.instancePosition.x;
	input.position.y += input.instancePosition.y;
	input.position.z += input.instancePosition.z;
	
	// calculate the position of the vertex against the world, view, and projection matrices
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	
	// store the texture coordinates for the pixel shader
	output.tex = input.tex;
	
	return output;
}

// pixel shader
float4 MultiTexturePixelShader(PixelInputType input) : SV_Target
{	
	// sample
	float4 textureColor1 = shaderTexture1.Sample(SampleType, input.tex);
    float4 textureColor2 = shaderTexture2.Sample(SampleType, input.tex);

    float4 blendColour = textureColor1 + textureColor2;
    blendColour = saturate(blendColour);

    return blendColour;
}

// technique
technique10 TextureTechnique
{
	pass pass0
	{
		SetVertexShader(CompileShader(vs_4_0, MultiTextureVertexShader()));
		SetPixelShader(CompileShader(ps_4_0, MultiTexturePixelShader()));
		SetGeometryShader(NULL);
	}
}
