#pragma once

#include <Engine\Graphics\Shaders\Shader.h>
#include <Engine\Utilities\ErrorOutput.h>

class MultiTextureShader : public Shader
{
private:
	struct MatrixBufferType
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX view;
		DirectX::XMMATRIX projection;
	};

public:
	void Initialise(ID3D11Device* device, HWND window);

	void Render(
		ID3D11DeviceContext* deviceContext,
		Mesh* mesh,
		DirectX::XMMATRIX* worldMatrix,
		DirectX::XMMATRIX* viewMatrix,
		DirectX::XMMATRIX* projectionMatrix,
		ID3D11ShaderResourceView* texture1,
		ID3D11ShaderResourceView* texture2);

private:
	bool InitialiseShader(
		ID3D11Device* device,
		HWND window,
		WCHAR* effectFilename);

	bool SetShaderParameters(
		DirectX::XMMATRIX* worldMatrix,
		DirectX::XMMATRIX* viewMatrix,
		DirectX::XMMATRIX* projectionMatrix,
		ID3D11ShaderResourceView* texture1,
		ID3D11ShaderResourceView* texture2);

	void RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount, int instanceCount);

	ID3DX11EffectShaderResourceVariable* mTexturePtr1;
	ID3DX11EffectShaderResourceVariable* mTexturePtr2;
};
