#include "TextureShader.h"

#include <Engine\Graphics\Meshes\Mesh.h>

#include <iostream>

void TextureShader::Initialise(ID3D11Device* device, HWND window, WCHAR* shaderFilename)
{
	bool result;

	// initialise the vertex and pixel shaders
	result = InitialiseShader(device, window, shaderFilename);
	if (!result)
	{
		ErrorOutput::Print("Error: Graphics manager", "Could not initialise the texture shader object");
	}
}

void TextureShader::Render(
	ID3D11DeviceContext* deviceContext,
	Mesh* mesh,
	DirectX::XMMATRIX* worldMatrix,
	DirectX::XMMATRIX* viewMatrix,
	DirectX::XMMATRIX* projectionMatrix)
{
	bool result;

	if (!mesh)
	{
		return;
	}

	// setup the mesh for rendering
	mesh->SetupForRender(deviceContext);

	// set the shader parameters that it will use for rendering
	result = SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, mesh->GetTexture());
	if (!result)
	{
		ErrorOutput::Print("Error: Texture shader", "Error setting shader parameters");
	}

	// now render the prepared buffers with the shader
	RenderShader(deviceContext, mesh->GetVertexCount(), mesh->GetInstanceCount());
}

bool TextureShader::InitialiseShader(
	ID3D11Device* device,
	HWND window,
	WCHAR* effectFilename)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;
	D3DX11_PASS_DESC passDesc;

	errorMessage = 0;

	// load the shader in from the file
	result = D3DX11CreateEffectFromFile(effectFilename, 0, device, &mEffect);
	if (FAILED(result))
	{
		return false;
	}

	// get a pointer to the technique inside the shader
	mTechnique = mEffect->GetTechniqueByName("TextureTechnique");
	if (!mTechnique)
	{
		return false;
	}

	// create the vertex input layout description
	// this setup needs to match the VertexType and InstanceType structures in the ModelClass and in the shader
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "TEXCOORD";
	polygonLayout[2].SemanticIndex = 1;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 1;
	polygonLayout[2].AlignedByteOffset = 0;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	polygonLayout[2].InstanceDataStepRate = 1;

	// get a count of the elements in the layout
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// get the description of the first pass described in the shader technique
	mTechnique->GetPassByIndex(0)->GetDesc(&passDesc);

	// create the input layout
	result = device->CreateInputLayout(polygonLayout, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &mLayout);
	if (FAILED(result))
	{
		return false;
	}

	// get pointers to the three matrices inside the shader so we can update them from this class
	mWorldMatrixPtr = mEffect->GetVariableByName("worldMatrix")->AsMatrix();
	mViewMatrixPtr = mEffect->GetVariableByName("viewMatrix")->AsMatrix();
	mProjectionMatrixPtr = mEffect->GetVariableByName("projectionMatrix")->AsMatrix();

	// get pointer to the texture resource inside the shader
	mTexturePtr = mEffect->GetVariableByName("shaderTexture")->AsShaderResource();

	if (mWorldMatrixPtr == nullptr ||
		mViewMatrixPtr == nullptr ||
		mProjectionMatrixPtr == nullptr ||
		mTexturePtr == nullptr)
	{
		return false;
	}

	return true;
}

bool TextureShader::SetShaderParameters(
	DirectX::XMMATRIX* worldMatrix,
	DirectX::XMMATRIX* viewMatrix,
	DirectX::XMMATRIX* projectionMatrix,
	ID3D11ShaderResourceView* texture)
{
	// set the matrix variables inside the shader
	mWorldMatrixPtr->SetMatrix(reinterpret_cast<float*>(worldMatrix));
	mViewMatrixPtr->SetMatrix(reinterpret_cast<float*>(viewMatrix));
	mProjectionMatrixPtr->SetMatrix(reinterpret_cast<float*>(projectionMatrix));

	// bind the texture
	mTexturePtr->SetResource(texture);

	return true;
}

void TextureShader::RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount, int instanceCount)
{
	D3DX11_TECHNIQUE_DESC techniqueDesc;
	
	// set the input layout
	deviceContext->IASetInputLayout(mLayout);

	// get the description structure of the technique from inside the shader so it can be used for rendering
	mTechnique->GetDesc(&techniqueDesc);

	// go through each pass in the technique and render the triangles
	for (unsigned int i = 0; i < techniqueDesc.Passes; i++)
	{
		mTechnique->GetPassByIndex(i)->Apply(0, deviceContext);
		deviceContext->DrawInstanced(vertexCount, instanceCount, 0, 0);
	}
}
