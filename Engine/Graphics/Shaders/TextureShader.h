#pragma once

#include <Engine\Graphics\Shaders\Shader.h>
#include <Engine\Utilities\ErrorOutput.h>

class TextureShader : public Shader
{
private:
	struct MatrixBufferType
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX view;
		DirectX::XMMATRIX projection;
	};

public:
	void Initialise(ID3D11Device* device, HWND window, WCHAR* shaderFilename);

	void Render(
		ID3D11DeviceContext* deviceContext,
		Mesh* mesh,
		DirectX::XMMATRIX* worldMatrix,
		DirectX::XMMATRIX* viewMatrix,
		DirectX::XMMATRIX* projectionMatrix);

private:
	bool InitialiseShader(
		ID3D11Device* device,
		HWND window,
		WCHAR* effectFilename);

	bool SetShaderParameters(
		DirectX::XMMATRIX* worldMatrix,
		DirectX::XMMATRIX* viewMatrix,
		DirectX::XMMATRIX* projectionMatrix,
		ID3D11ShaderResourceView* texture);

	void RenderShader(ID3D11DeviceContext* deviceContext, int vertexCount, int instanceCount);
};
