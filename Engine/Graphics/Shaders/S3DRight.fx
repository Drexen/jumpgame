// Globals
matrix worldMatrix;
matrix viewMatrix;
matrix projectionMatrix;
Texture2D shaderTexture;

// sample states
SamplerState SampleType
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

// type defs
struct VertexInputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 instancePosition : TEXCOORD1;
};

struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
};

// vertex shader
PixelInputType TextureVertexShader(VertexInputType input)
{
    PixelInputType output;
	
	// change the position vector to be 4 units for proper matrix calculations
    input.position.w = 1.0f;
	
	// calculate the position of the vertex against the world matrix
    output.position = mul(input.position, worldMatrix);

    // update the position of the vertices based on the data for this particular instance
    output.position.x += input.instancePosition.x;
    output.position.y += input.instancePosition.y;
    output.position.z += input.instancePosition.z;

    // calculate the position of the vertex against the view and projection matrices
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);
	
	// store the texture coordinates for the pixel shader
    output.tex = input.tex;
	
    return output;
}

// pixel shader
float4 TexturePixelShader(PixelInputType input) : SV_Target
{
	float4 textureColor;
	
	// sample the pixel color from the texture using the sampler at this texture coordinate location
	textureColor = shaderTexture.Sample(SampleType, input.tex);

    float4 finalColour = textureColor;

    float grayscale = dot(finalColour.rgb, float3(0.3, 0.59, 0.11));

    finalColour.r = 0;
    finalColour.g = grayscale;
    finalColour.b = grayscale;

    return finalColour;
}

// technique
technique10 TextureTechnique
{
	pass pass0
	{
		SetVertexShader(CompileShader(vs_4_0, TextureVertexShader()));
		SetPixelShader(CompileShader(ps_4_0, TexturePixelShader()));
		SetGeometryShader(NULL);
	}
}
