#include "GraphicsManager.h"

#include <string>
#include <iomanip>

#include <Engine\Core\InputManager.h>
#include <Engine\Graphics\Meshes\Mesh.h>
#include <Engine\Graphics\Models\ModelManager.h>
#include <Engine\Graphics\Textures\TextureManager.h>
#include <Engine\Utilities\ErrorOutput.h>
#include <Engine\Utilities\UsageUtilities.h>
#include <Engine\GameStates\GameStateManager.h>
#include <Engine\Core\constants.h>

bool GraphicsManager::Initialise(int screenWidth, int screenHeight, HWND window)
{
	HRESULT result;

	// Create the Direct3D object
	mD3DManager = std::make_unique<D3DManager>();
	if (!mD3DManager.get())
	{
		return false;
	}

	// Initialize the Direct3D object
	if (!mD3DManager->Initialise(screenWidth, screenHeight, VSYNC, window, FULLSCREEN, SCREEN_DEPTH, SCREEN_NEAR))
	{
		ErrorOutput::Print("Error: Graphics manager", "Could not initialise Direct3D.");
		return false;
	}

	// fw1 font factory
	result = FW1CreateFactory(FW1_VERSION, &mFontFactory);
	if (FAILED(result))
	{
		ErrorOutput::Print("Error: Graphics manager", "Could not initialise the FW1FontFactory.");
		return false;
	}

	std::string fontName = "ARIAL";
	result = mFontFactory->CreateFontWrapper(mD3DManager->GetDevice(), std::wstring(fontName.begin(), fontName.end()).c_str(), &mFontWrapper);
	if (FAILED(result))
	{
		ErrorOutput::Print("Error: Graphics manager", "Could not initialise font" + fontName);
		return false;
	}

	// texture manager
	TextureManager::Initialise(mD3DManager->GetDevice(), mD3DManager->GetDeviceContext(), window);
	
	return true;
}

void GraphicsManager::Shutdown()
{
	if (mD3DManager)
	{
		mD3DManager->Shutdown();
	}

	TextureManager::Shutdown();
	ModelManager::Shutdown();

	if (mFontFactory)
	{
		mFontFactory->Release();
	}

	if (mFontWrapper)
	{
		mFontWrapper->Release();
	}	
}

bool GraphicsManager::Render()
{
	// clear the buffers
	mD3DManager->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// render all active game states
	GameStateManager::Render();	

	// send the rendered scene to the screen
	mD3DManager->EndScene();
	
	return true;
}
