#pragma once

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <string>
#include <DirectXMath.h>

class RenderTexture;

class D3DManager
{
public:
	D3DManager();
	~D3DManager();

	bool Initialise(
		int screenWidth,
		int screenHeight,
		bool vsync,
		HWND window,
		bool fullscreen,
		float screenDepth,
		float screenNear);

	void Shutdown();

	void BeginScene(float red, float green, float blue, float alpha);
	void EndScene();

	ID3D11Device* GetDevice() { return mDevice; }
	ID3D11DeviceContext* GetDeviceContext() { return mDeviceContext; }

	DirectX::XMMATRIX GetProjectionMatrix() { return DirectX::XMMATRIX(XMLoadFloat4x4(&mProjectionMatrix)); }
	DirectX::XMMATRIX GetOrthoMatrix() { return DirectX::XMMATRIX(XMLoadFloat4x4(&mOrthoMatrix)); }
	DirectX::XMMATRIX GetViewMatrix2D() { return DirectX::XMMATRIX(XMLoadFloat4x4(&mViewMatrix2D)); }

	void TurnZBufferOn();
	void TurnZBufferOff();

	void TurnOnAlphaBlending();
	void TurnOffAlphaBlending();

	void SetRenderTargetTexture(RenderTexture* renderTexture);
	void SetRenderTargetBackBuffer();

private:
	bool mVSyncEnabled;
	int mVideoCardMemory;
	std::string mVideoCardDescription;

	IDXGISwapChain* mSwapChain;
	ID3D11Device* mDevice;
	ID3D11DeviceContext* mDeviceContext;
	ID3D11RenderTargetView* mRenderTargetView;

	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilState* mDepthStencilState;
	ID3D11DepthStencilState* mDepthDisabledStencilState;
	ID3D11DepthStencilView* mDepthStencilView;
	ID3D11RasterizerState* mRasterState;
	ID3D11BlendState* mAlphaEnableBlendingState;
	ID3D11BlendState* mAlphaDisableBlendingState;

	DirectX::XMFLOAT4X4 mProjectionMatrix;	
	DirectX::XMFLOAT4X4 mOrthoMatrix;
	DirectX::XMFLOAT4X4 mViewMatrix2D;
};
