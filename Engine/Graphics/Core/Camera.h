#pragma once

#include <DirectXMath.h>

class GameObject;

class Camera
{
public:

	enum CameraMode
	{
		CAMERA_FREE,
		CAMERA_FIXED,
		CAMERA_ATTACHED
	};

public:
	Camera();

	void SetPosition(DirectX::XMFLOAT3 position) { mPosition = position; }
	void SetRotation(DirectX::XMFLOAT3 rotation) { mRotation = rotation; }

	void Update();

	DirectX::XMMATRIX GetViewMatrix() { return DirectX::XMMATRIX(XMLoadFloat4x4(&mViewMatrix)); }

	DirectX::XMFLOAT3 GetPosition() { return mPosition; }
	DirectX::XMFLOAT3 GetRotation() { return mRotation; }
	DirectX::XMFLOAT3 GetForward() { return mForward; }

	void MoveLeft(float distance);
	void MoveRight(float distance);

	void SetCameraMode(CameraMode cameraMode) { mCameraMode = cameraMode; }
	void SetParent(GameObject* parent) { mParent = parent; }
	void SetParentOffset(DirectX::XMFLOAT3 offset) { mOffset = offset; }

private:
	void RecalculateViewMatrix();

	const float MOVE_SPEED = 10.0f;
	const float TURN_SPEED = 1.8f;

	DirectX::XMFLOAT4X4 mViewMatrix;

	DirectX::XMFLOAT3 mPosition, mForward, mRight, mUp;
	DirectX::XMFLOAT3 mRotation;

	DirectX::XMINT2 mMousePosition;

	GameObject* mParent;
	CameraMode mCameraMode;
	DirectX::XMFLOAT3 mOffset;
};
