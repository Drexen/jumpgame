#include "Camera.h"

#include <Engine\Core\InputManager.h>
#include <Engine\Utilities\MathUtilities.h>
#include <Engine\GameObjects\GameObject.h>
#include <Engine\Utilities\MathUtilities.h>

Camera::Camera()
{
	mPosition = { 0.0f, 0.0f, 0.0f };

	mForward = { 0.0f, 0.0f, 0.0f };
	mRight = { 0.0f, 0.0f, 0.0f };
	mUp = { 0.0f, 0.0f, 0.0f };

	mRotation = { 0.0f, 0.0f, 0.0f };

	mMousePosition = InputManager::GetMousePosition();

	mParent = nullptr;
	mCameraMode = CameraMode::CAMERA_FREE;
	mOffset = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
}

void Camera::Update()
{
	// gather input and update position and rotation vectors

	// fetch new mouse position
	DirectX::XMINT2 mousePositionNew = InputManager::GetMousePosition();
	DirectX::XMINT2 mouseDiff = SubtractSInt2(mousePositionNew, mMousePosition);

	InputManager::ResetMousePosition();
	mMousePosition = InputManager::GetMousePosition();

	// if camera mode is free, allow movement
	if (mCameraMode == CameraMode::CAMERA_FREE)
	{
		// forward
		if (InputManager::IsKeyPressed('W'))
		{
			mPosition = AddFloat3(mPosition, MultiplyFloat3(mForward, MOVE_SPEED));
		}

		// backward
		if (InputManager::IsKeyPressed('S'))
		{
			mPosition = SubtractFloat3(mPosition, MultiplyFloat3(mForward, MOVE_SPEED));
		}

		// strafe left
		if (InputManager::IsKeyPressed('A'))
		{
			mPosition = AddFloat3(mPosition, MultiplyFloat3(mRight, MOVE_SPEED));
		}

		// strafe right
		if (InputManager::IsKeyPressed('D'))
		{
			mPosition = SubtractFloat3(mPosition, MultiplyFloat3(mRight, MOVE_SPEED));
		}
	}
	else if (mCameraMode == CameraMode::CAMERA_ATTACHED)
	{
		// move camera to parent position + offset
		DirectX::XMFLOAT3 parentPos = mParent->GetBodyPosition();
		mPosition = AddFloat3(parentPos, mOffset);
	}

	// yaw
	mRotation.x += -(TURN_SPEED * mouseDiff.x * 0.05f);

	// pitch
	mRotation.y += -(TURN_SPEED * mouseDiff.y * 0.05f);	

	RecalculateViewMatrix();
}

void Camera::MoveLeft(float distance)
{
	mPosition = AddFloat3(mPosition, MultiplyFloat3(mRight, distance));

	RecalculateViewMatrix();
}

void Camera::MoveRight(float distance)
{
	mPosition = SubtractFloat3(mPosition, MultiplyFloat3(mRight, distance));

	RecalculateViewMatrix();
}

void Camera::RecalculateViewMatrix()
{
	// clamp rotation values
	do
	{
		if (mRotation.x < 0.0f)
		{
			mRotation.x += 360.0f;
		}
		if (mRotation.x > 360.0f)
		{
			mRotation.x -= 360.0f;
		}
	} while (mRotation.x < 0.0f || mRotation.x > 360.0f);


	if (mRotation.y < -90.0f)
	{
		mRotation.y = -90.0f;
	}
	if (mRotation.y > 90.0f)
	{
		mRotation.y = 90.0f;
	}

	// calculate sin/cos of mRotation vector
	DirectX::XMFLOAT3 rotRads =
	{
		DirectX::XMConvertToRadians(mRotation.x),
		DirectX::XMConvertToRadians(mRotation.y),
		DirectX::XMConvertToRadians(mRotation.z)
	};

	DirectX::XMFLOAT3 sinRotation = { sinf(rotRads.x), sinf(rotRads.y), sinf(rotRads.z) };
	DirectX::XMFLOAT3 cosRotation = { cosf(rotRads.x), cosf(rotRads.y), cosf(rotRads.z) };


	// recalculate forward & up vectors
	mForward.x = sinRotation.x * cosRotation.y;
	mForward.y = sinRotation.y;
	mForward.z = cosRotation.y * -cosRotation.x;

	mUp.x = (-cosRotation.x * sinRotation.z) - (sinRotation.x * sinRotation.y * cosRotation.z);
	mUp.y = cosRotation.y * cosRotation.z;
	mUp.z = (-sinRotation.x * sinRotation.z) - (sinRotation.y * cosRotation.z * -cosRotation.x);

	// cross product forward and up to get right vector

	//	i			j			k
	//	forward.x	forward.y	forward.z
	//	up.x		up.y		up.z

	mRight.x = (mForward.y * mUp.z) - (mForward.z * mUp.y);
	mRight.y = -((mForward.x * mUp.z) - (mForward.z * mUp.x));
	mRight.z = (mForward.x * mUp.y) - (mForward.y * mUp.x);

	// calculate lookAt vector
	DirectX::XMFLOAT3 lookAt = AddFloat3(mPosition, mForward);

	// create the view matrix from the three updated vectors
	XMStoreFloat4x4(&mViewMatrix, DirectX::XMMatrixLookAtLH(Float3ToVector(mPosition), Float3ToVector(lookAt), Float3ToVector(mUp)));
}
