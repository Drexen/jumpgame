#pragma once

#include <Engine\Graphics\Shaders\TextureShader.h>
#include <Engine\Graphics\Textures\RenderTexture.h>
#include <Engine\Graphics\Shaders\MultiTextureShader.h>
#include <memory>
#include <DirectXMath.h>

class GameObject;
class Camera;
class D3DManager;

class S3DRenderer
{
public:
	S3DRenderer() = default;
	
	void Initialise(D3DManager* d3dManager, HWND window, float eyeSeparation, float convergenceDistance);

	void ClearAll();

	void RenderLeft(Camera* camera, GameObject* object);
	void RenderRight(Camera* camera, GameObject* object);
	void RenderFinal(Mesh* meshScreenPlane);

	void Shutdown();

private:	

	std::unique_ptr<TextureShader> mShaderS3DLeft;
	std::unique_ptr<TextureShader> mShaderS3DRight;

	std::unique_ptr<RenderTexture> mRenderTextureLeft;
	std::unique_ptr<RenderTexture> mRenderTextureRight;
	std::unique_ptr<MultiTextureShader> mShaderMultiTexture;	

	D3DManager* mD3DManager;

	DirectX::XMFLOAT4X4 mProjectionMatrixS3DLeft;
	DirectX::XMFLOAT4X4 mProjectionMatrixS3DRight;

	float mEyeSeparation;
};
