#include "S3DRenderer.h"

#include <Engine\Graphics\Core\Camera.h>
#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\GameObjects\GameObject.h>
#include <Engine\Core\constants.h>

void S3DRenderer::Initialise(D3DManager* d3dManager, HWND window, float eyeSeparation, float convergenceDistance)
{
	mEyeSeparation = eyeSeparation;
	mD3DManager = d3dManager;

	// intiialise shaders
	mShaderS3DLeft = std::make_unique<TextureShader>();
	mShaderS3DLeft->Initialise(mD3DManager->GetDevice(), window, L"resources/shaders/S3DLeft.cso");

	mShaderS3DRight = std::make_unique<TextureShader>();
	mShaderS3DRight->Initialise(mD3DManager->GetDevice(), window, L"resources/shaders/S3DRight.cso");

	// left
	mRenderTextureLeft = std::make_unique<RenderTexture>();
	mRenderTextureLeft->Initialise(mD3DManager->GetDevice(), mD3DManager->GetDeviceContext(), SCREEN_WIDTH, SCREEN_HEIGHT);

	// right
	mRenderTextureRight = std::make_unique<RenderTexture>();
	mRenderTextureRight->Initialise(mD3DManager->GetDevice(), mD3DManager->GetDeviceContext(), SCREEN_WIDTH, SCREEN_HEIGHT);

	// combined
	mShaderMultiTexture = std::make_unique<MultiTextureShader>();
	mShaderMultiTexture->Initialise(mD3DManager->GetDevice(), window);

	// generate projection matricies
	float fov = DirectX::XMConvertToRadians(FOV_DEGREES);

	float fovHalf = fov / 2.0f;

	float aspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;

	float viewHeight = tanf(fovHalf) * SCREEN_NEAR;

	float a = tanf(fovHalf) * aspectRatio * convergenceDistance;
	float b = a - eyeSeparation;
	float c = a + eyeSeparation;

	float viewLeft = SCREEN_NEAR * b / convergenceDistance;
	float viewRight = SCREEN_NEAR * c / convergenceDistance;

	XMStoreFloat4x4(
		&mProjectionMatrixS3DLeft,
		DirectX::XMMatrixPerspectiveOffCenterLH(-viewLeft, viewRight, -viewHeight, viewHeight, SCREEN_NEAR, SCREEN_DEPTH));

	XMStoreFloat4x4(
		&mProjectionMatrixS3DRight,
		DirectX::XMMatrixPerspectiveOffCenterLH(-viewRight, viewLeft, -viewHeight, viewHeight, SCREEN_NEAR, SCREEN_DEPTH));
}

void S3DRenderer::ClearAll()
{
	mRenderTextureLeft->Clear(0.0f, 0.0f, 0.0f, 1.0f);
	mRenderTextureRight->Clear(0.0f, 0.0f, 0.0f, 1.0f);
}

void S3DRenderer::RenderLeft(Camera* camera, GameObject* object)
{
	// render left	
	mD3DManager->SetRenderTargetTexture(mRenderTextureLeft.get());

	camera->MoveLeft(mEyeSeparation);
	
	mShaderS3DLeft->Render(
		mD3DManager->GetDeviceContext(),
		object->GetMesh(),
		&object->GetWorldMatrix(),
		&camera->GetViewMatrix(),
		&XMLoadFloat4x4(&mProjectionMatrixS3DLeft));	

	camera->MoveRight(mEyeSeparation);

	mD3DManager->SetRenderTargetBackBuffer();
}

void S3DRenderer::RenderRight(Camera* camera, GameObject* object)
{
	// render right	
	mD3DManager->SetRenderTargetTexture(mRenderTextureRight.get());

	camera->MoveRight(mEyeSeparation);
	
	mShaderS3DRight->Render(
		mD3DManager->GetDeviceContext(),
		object->GetMesh(),
		&object->GetWorldMatrix(),
		&camera->GetViewMatrix(),
		&XMLoadFloat4x4(&mProjectionMatrixS3DRight));	

	camera->MoveLeft(mEyeSeparation);

	mD3DManager->SetRenderTargetBackBuffer();
}

void S3DRenderer::RenderFinal(Mesh* meshScreenPlane)
{
	mD3DManager->SetRenderTargetBackBuffer();

	mShaderMultiTexture->Render(
		mD3DManager->GetDeviceContext(),
		meshScreenPlane,
		&DirectX::XMMatrixIdentity(),
		&mD3DManager->GetViewMatrix2D(),
		&mD3DManager->GetOrthoMatrix(),
		mRenderTextureLeft->GetTexture(),
		mRenderTextureRight->GetTexture());
}

void S3DRenderer::Shutdown()
{
	if (mShaderS3DLeft)
	{
		mShaderS3DLeft->Shutdown();
	}

	if (mShaderS3DRight)
	{
		mShaderS3DRight->Shutdown();
	}

	if (mRenderTextureLeft)
	{
		mRenderTextureLeft->Shutdown();
	}

	if (mRenderTextureRight)
	{
		mRenderTextureRight->Shutdown();
	}

	if (mShaderMultiTexture)
	{
		mShaderMultiTexture->Shutdown();
	}
}
