#pragma once

#include <Windows.h>
#include <FW1FontWrapper.h>
#include <array>
#include <memory>

#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\Graphics\Core\Camera.h>
#include <Engine\Graphics\Shaders\TextureShader.h>

class GraphicsManager
{
	friend class Application;
public:
	GraphicsManager() = default;

	bool Initialise(int screenWidth, int screenHeight, HWND window);
	void Shutdown();

	IFW1FontWrapper* GetFontWrapper() { return mFontWrapper; }
	D3DManager* GetD3DManager() { return mD3DManager.get(); }

private:
	bool Render();

	std::unique_ptr<D3DManager> mD3DManager;	

	IFW1Factory* mFontFactory;
	IFW1FontWrapper* mFontWrapper;
};
