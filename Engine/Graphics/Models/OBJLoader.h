#pragma once

#include <vector>
#include <string>

#include <Engine\Graphics\Meshes\Mesh.h>

class OBJLoader
{
private:

	struct FaceIndices
	{
		FaceIndices(
			unsigned int v1, unsigned int uv1, unsigned int n1,
			unsigned int v2, unsigned int uv2, unsigned int n2,
			unsigned int v3, unsigned int uv3, unsigned int n3) :
		mVertex1(v1), mVertex2(v2), mVertex3(v3),
		mUV1(uv1), mUV2(uv2), mUV3(uv3),
		mNormal1(n1), mNormal2(n2), mNormal3(n3) {}

		unsigned int mVertex1, mVertex2, mVertex3;
		unsigned int mUV1, mUV2, mUV3;
		unsigned mNormal1, mNormal2, mNormal3;
	};

public:

	static void LoadOBJModel(std::vector<Mesh::VertexType>& vertices, std::string modelName);

private:
	OBJLoader() = default;
	~OBJLoader() = delete;
};
