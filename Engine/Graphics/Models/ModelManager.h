#pragma once

#include <d3d11.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <DirectXMath.h>

class Model;

class ModelManager
{
public:

	static void Shutdown();
	static Model* GetModel(std::string filename);

	static Model* DuplicateModel(std::string modelFilename, std::string duplicateFilename);
	static void ModifyModelVertexData(std::string filename, DirectX::XMMATRIX& transformMatrix);

private:

	ModelManager();

	static std::unordered_map<std::string, Model> mModels;
};
