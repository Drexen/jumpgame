#include "OBJLoader.h"

#include <fstream>
#include <string>
#include <vector>
#include <DirectXMath.h>
#include <regex>

#include <Engine\Utilities\ErrorOutput.h>

void OBJLoader::LoadOBJModel(std::vector<Mesh::VertexType>& vertices, std::string modelName)
{
	/*	OBJ file format:
	v = vertices (sets of 3 = 1 triangle)
	vt = texture coordinates (u/v)
	vn = normals (3 per triangle)
	f = faces    v/t/n v/t/n v/t/n		starts at 1
	*/

	std::ifstream file(modelName);
	std::string inString;

	// regex
	std::regex rVerts("v\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s*");
	std::smatch rMatchVerts;

	std::regex rTextureUVs("vt\\s+(\\S+)\\s+(\\S+).*");
	std::smatch rMatchTextureUVs;

	std::regex rNormals("vn\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)\\s*");
	std::smatch rMatchNormals;

	std::regex rFaces("f\\s+(\\d+)/(\\d+)/(\\d+)\\s+(\\d+)/(\\d+)/(\\d+)\\s+(\\d+)/(\\d+)/(\\d+).*");
	std::smatch rMatchFaces;

	// temp variables for storing model data
	std::vector<DirectX::XMFLOAT3> vVertices;
	std::vector<DirectX::XMFLOAT2> vTextureUVs;
	std::vector<DirectX::XMFLOAT3> vNormals;
	std::vector<FaceIndices> vFaceIndices;

	// read the file and collect model data
	while (getline(file, inString))
	{
		if (regex_match(inString, rMatchVerts, rVerts))
		{
			vVertices.push_back(DirectX::XMFLOAT3(stof(rMatchVerts[1]), stof(rMatchVerts[2]), stof(rMatchVerts[3])));
		}			
		else if (regex_match(inString, rMatchTextureUVs, rTextureUVs))
		{
			vTextureUVs.push_back(DirectX::XMFLOAT2(stof(rMatchTextureUVs[1]), -stof(rMatchTextureUVs[2])));
		}			
		else if (regex_match(inString, rMatchNormals, rNormals))
		{
			vNormals.push_back(DirectX::XMFLOAT3(stof(rMatchNormals[1]), stof(rMatchNormals[2]), stof(rMatchNormals[3])));
		}			
		else if (regex_match(inString, rMatchFaces, rFaces))
		{
			vFaceIndices.push_back(FaceIndices(
				stoi(rMatchFaces[1]), stoi(rMatchFaces[2]), stoi(rMatchFaces[3]),
				stoi(rMatchFaces[4]), stoi(rMatchFaces[5]), stoi(rMatchFaces[6]),
				stoi(rMatchFaces[7]), stoi(rMatchFaces[8]), stoi(rMatchFaces[9])));
		}
	}
	
	auto numVertices = vVertices.size();
	if (numVertices == 0)
	{
		std::string message = "Error loading vertices for model: " + modelName;
		ErrorOutput::Print("Error: OBJLoader", message);
	}

	auto numTextureUVs = vTextureUVs.size();
	if (numTextureUVs == 0)
	{
		std::string message = "Error loading texture UVs for model: " + modelName;
		ErrorOutput::Print("Error: OBJLoader", message);
	}

	auto numNormals = vNormals.size();
	if (numNormals == 0)
	{
		std::string message = "Error loading normals for model: " + modelName;
		ErrorOutput::Print("Error: OBJLoader", message);
	}

	auto numFaces = vFaceIndices.size();	
	if (numFaces == 0)
	{
		std::string message = "Error generating face data for model: " + modelName;
		ErrorOutput::Print("Error: OBJLoader", message);
	}

	//  generate VertexTypes for the shader from the vertex data, for every face (triangle)
	int count = 0;
	for (auto i = vFaceIndices.begin(); i != vFaceIndices.end(); i++)
	{
		// FaceIndices = v/t/n, v/t/n, v/t/n
		// making up 1 triangle

		vertices.push_back(Mesh::VertexType(vVertices[i->mVertex1 - 1], vTextureUVs[i->mUV1 - 1]));
		vertices.push_back(Mesh::VertexType(vVertices[i->mVertex2 - 1], vTextureUVs[i->mUV2 - 1]));
		vertices.push_back(Mesh::VertexType(vVertices[i->mVertex3 - 1], vTextureUVs[i->mUV3 - 1]));
	}

	// check if model loading failed
	if (vertices.size() == 0)
	{
		std::string message = "Error generating VertexTypes data for model: " + modelName;
		ErrorOutput::Print("Error: OBJLoader", message);
	}
}
