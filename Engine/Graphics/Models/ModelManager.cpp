#include "ModelManager.h"

#include <Engine\Graphics\Models\Model.h>
#include <Engine\Graphics\Models\OBJLoader.h>
#include <Engine\Utilities\ErrorOutput.h>

std::unordered_map<std::string, Model> ModelManager::mModels;

Model* ModelManager::GetModel(std::string filename)
{	
	if (!mModels[filename].loaded)
	{
		// since model not previously loaded, load it

		std::string filenameModel = filename + ".obj";
		std::string filenameTexture = filename + ".png";

		// load the model's vertex and indice data
		OBJLoader::LoadOBJModel(mModels[filename].mVertices, filenameModel);

		mModels[filename].loaded = true;
	}

	return &mModels[filename];
}

void ModelManager::Shutdown()
{
	// clear the texture map
	for (auto i = mModels.begin(); i != mModels.end();)
	{
		i->second.mVertices.clear();

		i = mModels.erase(i);
	}
}

Model* ModelManager::DuplicateModel(std::string modelFilename, std::string duplicateFilename)
{	
	if (modelFilename == duplicateFilename)
	{
		std::string message = "Cannot dupe model into a model with the same filename: " + modelFilename;
		ErrorOutput::Print("Model Manager", message);
		return nullptr;
	}

	if (mModels[duplicateFilename].loaded)
	{
		std::string message = "Model with dupe filename is already loaded: " + duplicateFilename;
		ErrorOutput::Print("Model Manager", message);
		return nullptr;
	}

	// if model isnt already loaded, load it
	if (!mModels[modelFilename].loaded)
	{
		GetModel(modelFilename);
	}

	// copy the model data into a duplicate model with different name
	Model* model = &mModels[modelFilename];
	mModels[duplicateFilename].mTexture = model->mTexture;
	mModels[duplicateFilename].mVertices = model->mVertices;

	mModels[duplicateFilename].loaded = true;

	return &mModels[duplicateFilename];
}

void ModelManager::ModifyModelVertexData(std::string filename, DirectX::XMMATRIX& transformMatrix)
{	
	// make sure the model is already loaded
	if (!mModels[filename].loaded)
	{
		std::string message = "Model to modify is not loaded: " + filename;
		ErrorOutput::Print("Model Manager", message);
		return;
	}

	for (auto i = mModels[filename].mVertices.begin(); i != mModels[filename].mVertices.end(); i++)
	{
		// transform the position data by the transform matrix			
		XMStoreFloat3(&(*i).mPosition, DirectX::XMVector3Transform(XMLoadFloat3(&((*i).mPosition)), transformMatrix));
	}	
}
