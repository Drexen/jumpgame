#pragma once

#include <vector>
#include <d3d11.h>

#include <Engine\Graphics\Meshes\Mesh.h>

class Model
{
public:

	Model() : loaded(false) {}

	ID3D11ShaderResourceView* mTexture;
	std::vector<Mesh::VertexType> mVertices;
	bool loaded;
};
