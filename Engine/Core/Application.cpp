#include "Application.h"

#include <Engine\Core\InputManager.h>
#include <Engine/Core/constants.h>
#include <Engine\Utilities\ErrorOutput.h>
#include <Engine\Utilities\RandomGenerator.h>
#include <Engine\GameStates\GameStateManager.h>
#include <Engine\Physics\Core\PhysicsManager.h>

bool Application::Initialise(std::unique_ptr<GameStates::GameState> initialGameState)
{
	int screenWidth;
	int screenHeight;

	mQuit = false;

	InitialiseWindows(screenWidth, screenHeight);

	// initialise the input manager
	InputManager::Initialise(mWindow, screenWidth, screenHeight);

	// initialise the random generator
	RandomGenerator::Initialise();

	// initialise ErrorOutput
	ErrorOutput::Initialise(mWindow);

	// create and initialise the graphics manager
	mGraphicsManager = std::make_unique<GraphicsManager>();
	if (!mGraphicsManager.get())
	{
		return false;
	}

	if (!mGraphicsManager->Initialise(screenWidth, screenHeight, mWindow))
	{
		return false;
	}

	mWindowHasFocus = true;

	// add the initial game state
	initialGameState->Initialise();
	initialGameState->SetActive();
	GameStateManager::Add(std::move(initialGameState));

	return true;
}

void Application::Shutdown()
{
	if (mGraphicsManager)
	{
		mGraphicsManager->Shutdown();
	}

	InputManager::Shutdown();

	GameStateManager::Shutdown();

	ShutdownWindows();
}

void Application::Run()
{
	MSG message;
	ZeroMemory(&message, sizeof(MSG));

	mQuit = false;
	while (!mQuit)
	{
		// handle windows messages
		if (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		// quit
		if (message.message == WM_QUIT)
		{
			mQuit = true;
		}
		else
		{
			if (mWindowHasFocus)
			{
				// frame processing
				if (!Update())
				{
					ErrorOutput::Print("Error: Application", "Frame processing failed.");
					mQuit;
				}
			}
		}		

		// check if escape is pressed to quit the application
		if (InputManager::IsKeyPressed(VK_ESCAPE))
		{
			mQuit = true;
		}
	}
}

LRESULT CALLBACK Application::MessageHandler(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{	
	if (message == WM_ACTIVATE)
	{
		if (LOWORD(wparam) == WA_ACTIVE)
		{
			mWindowHasFocus = true;
		}
		else
		{
			mWindowHasFocus = false;
		}
	}

	return DefWindowProc(window, message, wparam, lparam);
}

bool Application::Update()
{
	bool result;

	// update all active game states
	GameStateManager::Update();

	// render
	result = mGraphicsManager->Render();
	if (!result)
	{
		return false;
	}

	return true;
}

void Application::InitialiseWindows(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX windowClass;
	DEVMODE dmScreenSettings;
	int posX, posY;

	// get an external pointer to this object
	ApplicationHandle = this;

	// get the instance of this application
	mInstance = GetModuleHandle(0);

	// set application name
	mApplicationName = L"Engine";

	// setup the windows class with default settings
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = mInstance;
	windowClass.hIcon = LoadIcon(0, IDI_WINLOGO);
	windowClass.hIconSm = windowClass.hIcon;
	windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	windowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	windowClass.lpszMenuName = 0;
	windowClass.lpszClassName = mApplicationName;
	windowClass.cbSize = sizeof(WNDCLASSEX);

	// register the window class
	RegisterClassEx(&windowClass);

	// get screen resolution
	screenWidth = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// setup the screen settings
	if (FULLSCREEN)
	{
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner
		posX = posY = 0;
	}
	else
	{
		screenWidth = SCREEN_WIDTH;
		screenHeight = SCREEN_HEIGHT;

		// place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// create the window with the screen settings and get the handle to it
	mWindow = CreateWindowEx(
		WS_EX_APPWINDOW,
		mApplicationName,
		mApplicationName,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX,
		posY,
		screenWidth,
		screenHeight,
		0,
		0,
		mInstance,
		0);

	// bring the window up on the screen and set it as main focu
	ShowWindow(mWindow, SW_SHOW);
	SetForegroundWindow(mWindow);
	SetFocus(mWindow);

	// hide the mouse cursor
	ShowCursor(false);

	return;
}

void Application::ShutdownWindows()
{
	ShowCursor(true);

	// exit fullscreen
	if (FULLSCREEN)
	{
		ChangeDisplaySettings(0, 0);
	}

	// remove the window
	DestroyWindow(mWindow);
	mWindow = nullptr;

	// remove application instance
	UnregisterClass(mApplicationName, mInstance);
	mInstance = nullptr;

	ApplicationHandle = nullptr;
}

static LRESULT CALLBACK WndProc(HWND window, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_CLOSE:
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}

	default:
	{
		return ApplicationHandle->MessageHandler(window, message, wparam, lparam);
	}
	}
}
