#include "InputManager.h"

#include <memory>

#include <Engine\Utilities\ErrorOutput.h>

bool InputManager::mInitialised = false;

int InputManager::mScreenWidth;
int InputManager::mScreenHeight;

bool InputManager::Initialise(HWND window, int screenWidth, int screenHeight)
{
	if (mInitialised)
	{
		return true;
	}

	SetCapture(window);

	mScreenWidth = screenWidth;
	mScreenHeight = screenHeight;

	mInitialised = true;

	return true;	
}

DirectX::XMINT2 InputManager::ResetMousePosition()
{
	if (!mInitialised)
	{
		ErrorOutput::Print("Input Manager", "Not initialised.");
	}

	DirectX::XMINT2 mMousePos = { mScreenWidth / 2, mScreenHeight / 2 };
	SetCursorPos(mMousePos.x, mMousePos.y);

	return mMousePos;
}

void InputManager::Shutdown()
{
	ReleaseCapture();
}

DirectX::XMINT2 InputManager::GetMousePosition()
{
	if (!mInitialised)
	{
		ErrorOutput::Print("Input Manager", "Not initialised.");
	}

	std::unique_ptr<CURSORINFO> cursorInfo = std::make_unique<CURSORINFO>();
	cursorInfo->cbSize = sizeof(CURSORINFO);

	if (!GetCursorInfo(cursorInfo.get()))
	{
		DWORD error = GetLastError();
		ErrorOutput::Print("Error: Mouse input", "Error collecting cursor info.");
	}

	POINT mousePoint = cursorInfo->ptScreenPos;

	DirectX::XMINT2 mousePos = { (int32_t)mousePoint.x, (int32_t)mousePoint.y };

	return mousePos;
}

bool InputManager::IsKeyPressed(int key)
{
	if (!mInitialised)
	{
		ErrorOutput::Print("Input Manager", "Not initialised.");
	}

	if (GetKeyState(key) < 0)
	{
		return true;
	}

	return false;
}
