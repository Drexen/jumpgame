#pragma once

#include <Windows.h>
#include <memory>

#include <Engine\Graphics\Core\GraphicsManager.h>

static LRESULT CALLBACK WndProc(HWND window, UINT message, WPARAM wparam, LPARAM lparam);

namespace GameStates
{
	class GameState;
}

class Application
{
public:
	Application() = default;

	bool Initialise(std::unique_ptr<GameStates::GameState> initialGameState);
	void Shutdown();
	void Run();

	void QuitApplication() { mQuit = true; }

	GraphicsManager* GetGraphicsManager() { return mGraphicsManager.get(); }
	HWND GetWindow() { return mWindow; }

	LRESULT CALLBACK MessageHandler(HWND window, UINT message, WPARAM wparam, LPARAM lparam);

private:
	bool Update();
	void InitialiseWindows(int& screenWidth, int& screenHeight);
	void ShutdownWindows();

	LPCWSTR mApplicationName;
	HINSTANCE mInstance;
	HWND mWindow;

	bool mWindowHasFocus;

	std::unique_ptr<GraphicsManager> mGraphicsManager;

	bool mQuit;
};

static Application* ApplicationHandle = 0;
