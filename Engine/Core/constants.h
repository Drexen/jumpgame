#pragma once

constexpr unsigned int SCREEN_WIDTH = 1920;
constexpr unsigned int SCREEN_HEIGHT = 1080;

constexpr bool FULLSCREEN = false;
constexpr bool VSYNC = true;
constexpr float SCREEN_DEPTH = 100000.0f;
constexpr float SCREEN_NEAR = 0.01f;
constexpr float FOV_DEGREES = 70.0f;

constexpr bool S3D_ENABLED = false;
constexpr float EYE_SEPARATION = 5.0f;
constexpr float CONVERGENCE_DISTANCE = 600.0f;
