#pragma once

#include <DirectXMath.h>

#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <dinput.h>

class InputManager
{
public:

	static bool Initialise(HWND window, int screenWidth, int screenHeight);
	static void Shutdown();

	static bool IsKeyPressed(int key);
	static DirectX::XMINT2 GetMousePosition();

	static DirectX::XMINT2 ResetMousePosition();

private:

	InputManager() = default;

	static bool mInitialised;

	static int mScreenWidth;
	static int mScreenHeight;
};
