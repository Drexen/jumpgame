#include "PhysicsManager.h"

#include <Engine\GameObjects\GameObject.h>

void PhysicsManager::Initialise()
{
	// build the broadphase
	mBroadphase = std::make_unique<btDbvtBroadphase>();

	// setup the collision configuration and dispatcher
	mCollisionConfiguration = std::make_unique<btDefaultCollisionConfiguration>();
	mDispatcher = std::make_unique<btCollisionDispatcher>(mCollisionConfiguration.get());

	// physics solver
	mSolver = std::make_unique<btSequentialImpulseConstraintSolver>();

	// world
	mDynamicsWorld = std::make_unique<btDiscreteDynamicsWorld>(mDispatcher.get(), mBroadphase.get(), mSolver.get(), mCollisionConfiguration.get());
	mDynamicsWorld->setGravity(btVector3(0, GRAVITY, 0));
}

void PhysicsManager::Update(float deltaTime)
{

	// step physics simulation
	mDynamicsWorld->stepSimulation(deltaTime, PHYSICS_STEP_MAXIMUM);

	// handle collisions
	int numManifolds = mDynamicsWorld->getDispatcher()->getNumManifolds();

	for (int i = 0; i < numManifolds; i++)
	{
		btPersistentManifold* contactManifold = mDynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);

		int numContacts = contactManifold->getNumContacts();
		if (numContacts > 0)
		{
			const btCollisionObject* collisionObject1 = contactManifold->getBody0();
			const btCollisionObject* collisionObject2 = contactManifold->getBody1();

			GameObject* object1 = (GameObject*)(collisionObject1->getUserPointer());
			GameObject* object2 = (GameObject*)(collisionObject2->getUserPointer());	

			object1->Collision(object2);
			object2->Collision(object1);
		}
	}


}

void PhysicsManager::Shutdown()
{
	mDynamicsWorld.reset();

	mDispatcher.reset();
	mBroadphase.reset();
	mCollisionConfiguration.reset();	
	mSolver.reset();	
}
