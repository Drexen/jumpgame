#pragma once

#include <btBulletDynamicsCommon.h>
#include <memory>

constexpr int PHYSICS_STEP_MAXIMUM = 10;

class PhysicsManager
{
public:

	const float GRAVITY = -750.0f;

public:
	PhysicsManager() = default;

	void Initialise();

	void AddBody(btRigidBody* body) { mDynamicsWorld->addRigidBody(body); }
	void RemoveBody(btRigidBody* body) { mDynamicsWorld->removeRigidBody(body); }

	btDiscreteDynamicsWorld* GetWorld() { return mDynamicsWorld.get(); }

	void Update(float deltaTime);

	void Shutdown();

private:	

	std::unique_ptr<btBroadphaseInterface> mBroadphase;
	std::unique_ptr<btDefaultCollisionConfiguration> mCollisionConfiguration;
	std::unique_ptr<btCollisionDispatcher> mDispatcher;
	std::unique_ptr<btSequentialImpulseConstraintSolver> mSolver;

	std::unique_ptr<btDiscreteDynamicsWorld> mDynamicsWorld;
};
