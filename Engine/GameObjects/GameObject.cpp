#include "GameObject.h"

#include <Engine\GameStates\GameState.h>

GameObject::GameObject(GameStates::GameState* gameState) :
	mGameState(gameState)
{
	mPosition = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	mRotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	mScale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);

	// set default world matrix to identity
	XMStoreFloat4x4(&mWorldMatrix, DirectX::XMMatrixIdentity());
}

void GameObject::InitialiseMesh(Mesh* mesh)
{
	// move ownership of the mesh to this object
	mMesh.reset(mesh);
}

void GameObject::InitialiseCollisions(DirectX::XMFLOAT3 pos, float mass, btCollisionShape* shape)
{
	// body
	btVector3 inertia(0, 0, 0);

	btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(pos.x, pos.y, pos.z)));

	shape->calculateLocalInertia(mass, inertia);

	btRigidBody::btRigidBodyConstructionInfo bodyConstructionInfo(mass, motionState, shape, inertia);
	mBody = std::make_unique<btRigidBody>(bodyConstructionInfo);

	// initialise the world matrix
	UpdateWorldMatrix();
	
	// set user pointer
	mBody->setUserPointer(this);
}

void GameObject::Update()
{
	UpdateWorldMatrix();
}

void GameObject::UpdateWorldMatrix()
{
	btVector3 bodyPosition = btVector3(0.0f, 0.0f, 0.0f);

	// update position if body exists
	if (mBody.get())
	{
		// get body position
		btTransform bodyTransform;
		mBody->getMotionState()->getWorldTransform(bodyTransform);

		bodyPosition = bodyTransform.getOrigin();
	}

	DirectX::XMMATRIX transform = DirectX::XMMatrixIdentity();
	DirectX::XMMATRIX scale = DirectX::XMMatrixScaling(mScale.x, mScale.y, mScale.z);
	DirectX::XMMATRIX rotation = DirectX::XMMatrixRotationRollPitchYaw(
		DirectX::XMConvertToRadians(mRotation.x),
		DirectX::XMConvertToRadians(mRotation.y),
		DirectX::XMConvertToRadians(mRotation.z));

	DirectX::XMMATRIX translation = DirectX::XMMatrixTranslation(
		mPosition.x + bodyPosition.getX(),
		mPosition.y + bodyPosition.getY(),
		mPosition.z + bodyPosition.getZ());

	transform = DirectX::XMMatrixMultiply(transform, scale);
	transform = DirectX::XMMatrixMultiply(transform, rotation);
	transform = DirectX::XMMatrixMultiply(transform, translation);

	XMStoreFloat4x4(&mWorldMatrix, transform);
}

void GameObject::Collision(GameObject* collider)
{

}

void GameObject::Shutdown()
{
	if (mMesh)
	{
		mMesh->Shutdown();
	}

	if (mBody)
	{
		mGameState->GetPhysicsManager()->RemoveBody(mBody.get());
	}
}

DirectX::XMFLOAT3 GameObject::GetBodyPosition()
{
	// get body position
	btTransform transform;
	mBody->getMotionState()->getWorldTransform(transform);

	btVector3 pos = transform.getOrigin();

	return DirectX::XMFLOAT3(pos.getX(), pos.getY(), pos.getZ());
}
