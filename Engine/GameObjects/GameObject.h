#pragma once

#include <Engine\Graphics\Meshes\Mesh.h>
#include <btBulletDynamicsCommon.h>
#include <memory>

namespace GameStates
{
	class GameState;
}

class GameObject
{
public:
	GameObject(GameStates::GameState* gameState);

	void InitialiseMesh(Mesh* mesh);
	void InitialiseCollisions(DirectX::XMFLOAT3 pos, float mass, btCollisionShape* shape);

	Mesh* GetMesh() { return mMesh.get(); }
	btRigidBody* GetBody() { return mBody.get(); }
	DirectX::XMFLOAT3 GetBodyPosition();

	DirectX::XMMATRIX GetWorldMatrix() { return XMLoadFloat4x4(&mWorldMatrix); }

	void SetMeshPosition(DirectX::XMFLOAT3 in) { mPosition = in; }
	void SetMeshRotation(DirectX::XMFLOAT3 in) { mRotation = in; }
	void SetMeshScale(DirectX::XMFLOAT3 in) { mScale = in; }

	virtual void Collision(GameObject* collider);

	virtual void Update();

	void Shutdown();

protected:

	std::unique_ptr<btRigidBody> mBody;

	GameStates::GameState* mGameState;

private:

	void UpdateWorldMatrix();

	std::unique_ptr<Mesh> mMesh;

	DirectX::XMFLOAT3 mPosition;
	DirectX::XMFLOAT3 mRotation;
	DirectX::XMFLOAT3 mScale;

	DirectX::XMFLOAT4X4 mWorldMatrix;	
};
