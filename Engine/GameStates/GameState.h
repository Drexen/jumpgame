#pragma once

#include <Engine\Physics\Core\PhysicsManager.h>
#include <Engine\GameObjects\GameObject.h>
#include <Engine\Graphics\Core\S3DRenderer.h>
#include <Engine\Graphics\Core\Camera.h>
#include <Engine\Graphics\Core\GraphicsManager.h>
#include <Engine\Core\Application.h>
#include <FW1FontWrapper.h>

class Application;

namespace GameStates
{
	class GameState
	{
	public:
		GameState(Application* application);

		virtual void Initialise();

		virtual void Update();

		virtual void Render(float clearRed, float clearGreen, float clearBlue);
		
		virtual void Shutdown();

		bool IsActive() { return mActive; }
		virtual void SetActive() { mActive = true; }
		virtual void SetInactive() { mActive = false; }

		PhysicsManager* GetPhysicsManager() { return mPhysicsManager.get(); }
		D3DManager* GetD3DManager() { return mD3DManager; }

		GameObject* AddGameObject(std::unique_ptr<GameObject> object);
		void RemoveGameObject(GameObject* object);

	protected:
		Application* mApplication;
		D3DManager* mD3DManager;
		IFW1FontWrapper* mFontWrapper;
		std::unique_ptr<Camera> mCamera;

	private:
		bool mActive;

		std::unique_ptr<PhysicsManager> mPhysicsManager;		
		std::vector<std::unique_ptr<GameObject>> mGameObjects;

		std::unique_ptr<S3DRenderer> mS3DRenderer;
		std::unique_ptr<TextureShader> mShaderTexture;
		std::unique_ptr<Mesh> mMeshPlane;
	};
}
