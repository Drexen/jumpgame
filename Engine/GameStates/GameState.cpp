#include "GameState.h"

#include <Engine\Graphics\Textures\TextureManager.h>
#include <Engine\Core\constants.h>
#include <Engine\Utilities\ErrorOutput.h>

GameStates::GameState::GameState(Application* application)
{
	mActive = false;
	mApplication = application;	
}

void GameStates::GameState::Initialise()
{
	mD3DManager = mApplication->GetGraphicsManager()->GetD3DManager();
	mFontWrapper = mApplication->GetGraphicsManager()->GetFontWrapper();

	// physics
	mPhysicsManager = std::make_unique<PhysicsManager>();
	mPhysicsManager->Initialise();

	// camera object
	mCamera = std::make_unique<Camera>();
	mCamera->SetPosition(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera->SetRotation(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCamera->Update();

	// texture shader
	mShaderTexture = std::make_unique<TextureShader>();
	mShaderTexture->Initialise(mD3DManager->GetDevice(), mApplication->GetWindow(), L"resources/shaders/Texture.cso");

	// S3D renderer
	mS3DRenderer = std::make_unique<S3DRenderer>();
	mS3DRenderer->Initialise(mD3DManager, mApplication->GetWindow(), EYE_SEPARATION, CONVERGENCE_DISTANCE);

	// screen plane mesh
	mMeshPlane = std::make_unique<Mesh>();
	Mesh::MeshData meshDataPlane;
	meshDataPlane.overrideTexture = TextureManager::GetTextureFromPNG("resources/white.png");
	meshDataPlane.vertexData = Mesh::GenerateVertices2DPlane(SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	meshDataPlane.pos = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	mMeshPlane->Initialise(mD3DManager->GetDevice(), meshDataPlane);
}

void GameStates::GameState::Update()
{
	mPhysicsManager->Update(1.0f / 60.0f);

	for (auto i = mGameObjects.begin(); i != mGameObjects.end(); i++)
	{
		(*i)->Update();
	}
}

void GameStates::GameState::Render(float clearRed, float clearGreen, float clearBlue)
{
	// 3D
	// update camera position
	mCamera->Update();

	mD3DManager->BeginScene(clearRed, clearGreen, clearBlue, 1.0f);

	if (S3D_ENABLED)
	{
		// clear all
		mS3DRenderer->ClearAll();

		for (auto i = mGameObjects.begin(); i != mGameObjects.end(); i++)
		{
			// render left
			mS3DRenderer->RenderLeft(mCamera.get(), (*i).get());

			// render right
			mS3DRenderer->RenderRight(mCamera.get(), (*i).get());
		}

		// render main
		mS3DRenderer->RenderFinal(mMeshPlane.get());
	}
	else
	{
		for (auto i = mGameObjects.begin(); i != mGameObjects.end(); i++)
		{
			mShaderTexture->Render(
				mD3DManager->GetDeviceContext(),
				(*i)->GetMesh(),
				&(*i)->GetWorldMatrix(),
				&mCamera->GetViewMatrix(),
				&mD3DManager->GetProjectionMatrix());
		}
	}
}

GameObject* GameStates::GameState::AddGameObject(std::unique_ptr<GameObject> object)
{
	if (object->GetBody())
	{
		mPhysicsManager->AddBody(object->GetBody());
	}

	mGameObjects.push_back(std::move(object));	

	return mGameObjects.back().get();
}

void GameStates::GameState::RemoveGameObject(GameObject* object)
{
	for(auto i = mGameObjects.begin(); i != mGameObjects.end(); i++)
	{
		if (object == (*i).get())
		{
			(*i)->Shutdown();			
			mGameObjects.erase(i);
			break;
		}
	}
}

void GameStates::GameState::Shutdown()
{
	// shutdown all game objects
	for (auto i = mGameObjects.begin(); i != mGameObjects.end(); i++)
	{
		if (*i)
		{
			(*i)->Shutdown();
		}
	}

	if (mMeshPlane)
	{
		mMeshPlane->Shutdown();
	}

	// shutdown the physics manager
	mPhysicsManager->Shutdown();
}
