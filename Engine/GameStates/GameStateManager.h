#pragma once

#include <deque>

#include <Engine\GameStates\GameState.h>

class GameStateManager
{
public:
	static void Add(std::unique_ptr<GameStates::GameState> in);
	static bool Remove(GameStates::GameState* in);
	static bool RemoveCurrent();

	static GameStates::GameState* GetCurrent();

	static void Update();

	static void Render();

	static void Shutdown();

private:
	GameStateManager() = default;	

	static std::deque<std::unique_ptr<GameStates::GameState>> mGameStates;
};
