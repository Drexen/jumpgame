#include "GameStateManager.h"

std::deque<std::unique_ptr<GameStates::GameState>> GameStateManager::mGameStates;

void GameStateManager::Add(std::unique_ptr<GameStates::GameState> in)
{
	mGameStates.push_front(std::move(in));
}

bool GameStateManager::Remove(GameStates::GameState* in)
{
	for (auto it = mGameStates.begin(); it != mGameStates.end(); it++)
	{
		if (it->get() == in)
		{
			(*it)->SetInactive();
			(*it)->Shutdown();
			mGameStates.erase(it);
			return true;
		}
	}

	return false;
}

bool GameStateManager::RemoveCurrent()
{
	if (mGameStates.size() == 0)
	{
		return false;
	}

	mGameStates.front()->SetInactive();
	mGameStates.front()->Shutdown();
	mGameStates.pop_front();
	return true;
}

GameStates::GameState* GameStateManager::GetCurrent()
{
	if (mGameStates.size() == 0)
	{
		return nullptr;
	}

	return &*mGameStates.front();
}

void GameStateManager::Update()
{
	// copy the game states vector
	std::vector<GameStates::GameState*> gameStates;
	for (auto i = mGameStates.begin(); i != mGameStates.end(); i++)
	{
		gameStates.push_back((*i).get());
	}

	// update all active game states
	for (auto i = gameStates.begin(); i != gameStates.end(); i++)
	{
		if ((*i)->IsActive())
		{
			(*i)->Update();
		}
	}
}

void GameStateManager::Render()
{
	// render all active game states
	for (auto i = mGameStates.begin(); i != mGameStates.end(); i++)
	{
		if ((*i)->IsActive())
		{
			(*i)->Render(0.0f, 0.0f, 0.0f);
		}
	}
}

void GameStateManager::Shutdown()
{
	// shutdown all active game states
	for (auto i = mGameStates.begin(); i != mGameStates.end(); i++)
	{
		(*i)->Shutdown();
	}
}
