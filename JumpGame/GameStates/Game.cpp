#include <JumpGame\GameStates\Game.h>

#include <Engine\Graphics\Core\GraphicsManager.h>
#include <Engine\Utilities\RandomGenerator.h>
#include <Engine\Utilities\ErrorOutput.h>

#include <JumpGame\GameObjects\Player.h>

GameStates::Game::Game(Application* application) :
	GameStates::GameState(application)
{
	maxPlayerDistance = 0.0f;
}

void GameStates::Game::Initialise()
{
	GameState::Initialise();
	
	// player
	std::unique_ptr<Player> player = std::make_unique<Player>(
		this,
		mD3DManager,
		mCamera.get(),
		DirectX::XMFLOAT3(-100.0f, 200.0f, 0.0f));
	mPlayer = player.get();
	AddGameObject(std::move(player));

	// initialise camera
	//mCamera->SetCameraMode(Camera::CameraMode::CAMERA_FREE);
	//mCamera->SetPosition(DirectX::XMFLOAT3(-400.0f, 100.0f, 0.0f));
	//mCamera->SetRotation(DirectX::XMFLOAT3(90.0f, 0.0f, 0.0f));
	mCamera->SetCameraMode(Camera::CameraMode::CAMERA_ATTACHED);
	mCamera->SetParent(mPlayer);
	mCamera->SetParentOffset(DirectX::XMFLOAT3(0.0f, 25.0f, 0.0f));
	mCamera->SetRotation(DirectX::XMFLOAT3(90.0f, 0.0f, 0.0f));

	float xOffset = 600.0f;
	float xRange = 120.0f;
	float yRange = 100.0f;
	float zRange = 500.0f;

	// hanging platforms
	DirectX::XMFLOAT3 currentPlatformPos = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < NUM_PLATFORMS; i++)
	{
		std::unique_ptr<HangingPlatform> platform = std::make_unique<HangingPlatform>();
		platform->Initialise(this, currentPlatformPos);
		mHangingPlatforms.push_back(std::move(platform));

		currentPlatformPos.x += RandomGenerator::GetRandomFloat(-xRange, xRange);
		currentPlatformPos.x += xOffset;

		// tend towards platforms being higher than lower
		currentPlatformPos.y += RandomGenerator::GetRandomFloat(-yRange * 0.6f, yRange * 1.2f);
		currentPlatformPos.z += RandomGenerator::GetRandomFloat(-zRange, zRange);

		xOffset += X_OFFSET_INCREMENT;

		xRange += X_RANGE_INCREMENT;
		yRange += Y_RANGE_INCREMENT;
		zRange += Z_RANGE_INCREMENT;

		if (i == NUM_PLATFORMS - 1)
		{
			xWinDistance = currentPlatformPos.x;
		}
	}
}

void GameStates::Game::Update()
{
	GameStates::GameState::Update();

	// check if player fell out of world
	// get body position
	btTransform transform;
	mPlayer->GetBody()->getMotionState()->getWorldTransform(transform);
	
	btVector3 pos = transform.getOrigin();
	
	if (pos.getY() < OUT_OF_WORLD_Y)
	{
		EndGame(false);
	}
	
	// check if player reached win distance
	if (pos.getX() > xWinDistance)
	{
		EndGame(true);
	}
	
	// update maxPlayerDistance
	if (pos.getX() > maxPlayerDistance)
	{
		maxPlayerDistance = pos.getX();
	}
}

void GameStates::Game::EndGame(bool win)
{
	// display final score
	int score = (int)std::roundf(maxPlayerDistance);

	std::string message = "Score: " + std::to_string((int)score);
	std::string title;
	if (win)
	{
		title = "Game WON!";
	}
	else
	{
		title = "Game Over";
	}

	ErrorOutput::Print(title, message);

	// quit application
	mApplication->QuitApplication();
}

void GameStates::Game::Render(float clearRed, float clearGreen, float clearBlue)
{
	GameStates::GameState::Render(0.3f, 0.3f, 0.3f);

	// 2D
	mFontWrapper->DrawString(mD3DManager->GetDeviceContext(), L"Jump Game", 20.0f, 10.0f, 10.0f, 0xffffffff, FW1_RESTORESTATE);

	std::wstring wstring;
	wstring = L"Score: " + std::to_wstring((int)roundf(maxPlayerDistance));

	mFontWrapper->DrawString(mD3DManager->GetDeviceContext(), wstring.c_str(), 20.0f, 10.0f, 70.0f, 0xffffffff, FW1_RESTORESTATE);
}

void GameStates::Game::Shutdown()
{
	// delete player
	mCamera->SetParent(nullptr);
	mCamera->SetCameraMode(Camera::CAMERA_FIXED);

	RemoveGameObject(mPlayer);
	mPlayer = nullptr;
}
