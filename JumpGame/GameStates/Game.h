#pragma once

#include <Engine\GameStates\GameState.h>
#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\Core\Application.h>
#include <Engine\Graphics\Models\ModelManager.h>
#include <Engine\Graphics\Meshes\Mesh.h>

#include <JumpGame\Prefabs\HangingPlatform.h>

class Player;

namespace GameStates
{
	class Game : public GameState
	{
	public:

		const float X_RANGE_INCREMENT = 0.02f;
		const float Y_RANGE_INCREMENT = 0.06f;
		const float Z_RANGE_INCREMENT = 0.12f;

		const float X_OFFSET_INCREMENT = 0.7f;

		const float OUT_OF_WORLD_Y = -950.0f;

		const int NUM_PLATFORMS = 200;

	public:
		Game(Application* application);

		void Initialise();

		void Update();

		void Render(float clearRed, float clearGreen, float clearBlue);

		void Shutdown();

	private:

		void EndGame(bool win);

		float maxPlayerDistance;

		Player* mPlayer;
		std::vector<std::unique_ptr<HangingPlatform>> mHangingPlatforms;

		float xWinDistance;
	};
}
