#include <JumpGame\GameStates\MainMenu.h>

#include <Engine\Core\InputManager.h>
#include <Engine\GameStates\GameStateManager.h>
#include <JumpGame\GameStates\Game.h>

GameStates::MainMenu::MainMenu(Application* application) :
	GameStates::GameState(application)
{

}

void GameStates::MainMenu::Initialise()
{
	GameState::Initialise();

	// setup camera
	mCamera->SetPosition(DirectX::XMFLOAT3(115.0f, -50.0f, -150.0f));
	mCamera->SetRotation(DirectX::XMFLOAT3(180.0f, 0.0f, 0.0f));
	mCamera->SetCameraMode(Camera::CameraMode::CAMERA_FIXED);

	// E
	Mesh* meshLetterE = new Mesh();
	Mesh::MeshData meshDataLetterE;
	std::vector<Mesh::InstanceType> instancesLetterE;
	instancesLetterE.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)));
	instancesLetterE.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(130.0f, 0.0f, 0.0f)));
	meshDataLetterE.instanceData = instancesLetterE;

	meshDataLetterE.modelName = "resources/models/alphabet/letter_E";
	meshDataLetterE.overrideTextureName = "resources/white.png";

	meshLetterE->Initialise(mD3DManager->GetDevice(), meshDataLetterE);

	std::unique_ptr<GameObject> objectLetterE= std::make_unique<GameObject>(this);
	objectLetterE->InitialiseMesh(meshLetterE);	
	AddGameObject(std::move(objectLetterE));

	// N
	Mesh* meshLetterN = new Mesh();
	Mesh::MeshData meshDataLetterN;
	meshDataLetterN.pos = DirectX::XMFLOAT3(40.0f, 0.0f, 0.0f);
	meshDataLetterN.modelName = "resources/models/alphabet/letter_N";
	meshDataLetterN.overrideTextureName = "resources/white.png";

	meshLetterN->Initialise(mD3DManager->GetDevice(), meshDataLetterN);

	std::unique_ptr<GameObject> objectLetterN = std::make_unique<GameObject>(this);
	objectLetterN->InitialiseMesh(meshLetterN);
	AddGameObject(std::move(objectLetterN));

	// T
	Mesh* meshLetterT = new Mesh();
	Mesh::MeshData meshDataLetterT;
	meshDataLetterT.pos = DirectX::XMFLOAT3(90.0f, 0.0f, 0.0f);
	meshDataLetterT.modelName = "resources/models/alphabet/letter_T";
	meshDataLetterT.overrideTextureName = "resources/white.png";

	meshLetterT->Initialise(mD3DManager->GetDevice(), meshDataLetterT);

	std::unique_ptr<GameObject> objectLetterT = std::make_unique<GameObject>(this);
	objectLetterT->InitialiseMesh(meshLetterT);
	AddGameObject(std::move(objectLetterT));

	// R
	Mesh* meshLetterR = new Mesh();
	Mesh::MeshData meshDataLetterR;
	meshDataLetterR.pos = DirectX::XMFLOAT3(170.0f, 0.0f, 0.0f);
	meshDataLetterR.modelName = "resources/models/alphabet/letter_R";
	meshDataLetterR.overrideTextureName = "resources/white.png";

	meshLetterR->Initialise(mD3DManager->GetDevice(), meshDataLetterR);

	std::unique_ptr<GameObject> objectLetterR = std::make_unique<GameObject>(this);
	objectLetterR->InitialiseMesh(meshLetterR);
	AddGameObject(std::move(objectLetterR));
}

void GameStates::MainMenu::Update()
{
	GameStates::GameState::Update();

	// switch to game
	if (InputManager::IsKeyPressed(VK_RETURN))
	{		
		std::unique_ptr<GameStates::Game> game = std::make_unique<GameStates::Game>(mApplication);
		game->Initialise();
		game->SetActive();

		GameStateManager::Add(std::move(game));
		GameStateManager::Remove(this);
	}
}

void GameStates::MainMenu::Render(float clearRed, float clearGreen, float clearBlue)
{
	GameStates::GameState::Render(0.0f, 0.0f, 0.0f);
	
	// 2D
	mFontWrapper->DrawString(mD3DManager->GetDeviceContext(), L"Jump Game", 20.0f, 10.0f, 10.0f, 0xffffffff, FW1_RESTORESTATE);
}
