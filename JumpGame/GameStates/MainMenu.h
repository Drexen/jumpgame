#pragma once

#include <Engine\GameStates\GameState.h>
//#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\Core\Application.h>
//#include <Engine\Graphics\Models\ModelManager.h>
//#include <Engine\Graphics\Meshes\Mesh.h>

namespace GameStates
{
	class MainMenu : public GameState
	{
	public:
		MainMenu(Application* application);

		void Initialise();

		void Update();

		void Render(float clearRed, float clearGreen, float clearBlue);
	};
}
