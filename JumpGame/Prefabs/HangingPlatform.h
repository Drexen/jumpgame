#pragma once

#include <Engine\GameObjects\GameObject.h>
#include <Engine\Graphics\Meshes\Mesh.h>
#include <Engine\Graphics\Core\D3DManager.h>
#include <Engine\GameStates\GameState.h>

class HangingPlatform
{
public:

	HangingPlatform() = default;

	void Initialise(GameStates::GameState* gameState, DirectX::XMFLOAT3 pos);

	void Shutdown();

private:

	GameStates::GameState* mGameState;

	Mesh* mMeshChains;
	Mesh::MeshData* mMeshDataChains;
	GameObject* mObjectChains;

	Mesh* mMeshCeilingRocks;
	Mesh::MeshData* mMeshDataCeilingRocks;
	GameObject* mObjectCeilingRocks;

	Mesh* mMeshCliff1;
	Mesh::MeshData* mMeshDataCliff1;
	GameObject* mObjectCliff1;

	Mesh* mMeshCliff2;
	Mesh::MeshData* mMeshDataCliff2;
	GameObject* mObjectCliff2;

	Mesh* mMeshPlatform;
	Mesh::MeshData* mMeshDataPlatform;
	GameObject* mObjectPlatform;
};
