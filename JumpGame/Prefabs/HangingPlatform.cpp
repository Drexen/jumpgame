#include "HangingPlatform.h"

#include <Engine\Utilities\MathUtilities.h>

void HangingPlatform::Initialise(GameStates::GameState* gameState, DirectX::XMFLOAT3 pos)
{
	mGameState = gameState;

	// chains
	Mesh* mMeshChains = new Mesh();
	mMeshDataChains = new Mesh::MeshData();

	std::vector<Mesh::InstanceType> meshChainsInstances;

	float chainX = 148.0f;
	float chainZ = 135.0f;
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(chainX, 360.0f, chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(chainX, 360.0f, -chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(-chainX, 360.0f, chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(-chainX, 360.0f, -chainZ)));

	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(chainX, 720.0f, chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(chainX, 720.0f, -chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(-chainX, 720.0f, chainZ)));
	meshChainsInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(-chainX, 720.0f, -chainZ)));
	mMeshDataChains->instanceData = meshChainsInstances;

	mMeshDataChains->modelName = "resources/models/chain";
	
	mMeshChains->Initialise(gameState->GetD3DManager()->GetDevice(), *mMeshDataChains);

	std::unique_ptr<GameObject> objectChains = std::make_unique<GameObject>(gameState);
	objectChains->InitialiseMesh(mMeshChains);
	objectChains->SetMeshRotation(DirectX::XMFLOAT3(90.0f, 0.0f, 0.0f));
	objectChains->SetMeshPosition(pos);
	mObjectChains = gameState->AddGameObject(std::move(objectChains));

	// ceiling rocks
	Mesh* mMeshCeilingRocks = new Mesh();
	mMeshDataCeilingRocks = new Mesh::MeshData();

	std::vector<Mesh::InstanceType> meshCeilingRocksInstances;
	meshCeilingRocksInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 880.0f, 0.0f)));
	mMeshDataCeilingRocks->instanceData = meshCeilingRocksInstances;

	mMeshDataCeilingRocks->modelName = "resources/models/mine_rocks";

	mMeshCeilingRocks->Initialise(gameState->GetD3DManager()->GetDevice(), *mMeshDataCeilingRocks);

	std::unique_ptr<GameObject> objectCeilingRocks = std::make_unique<GameObject>(gameState);
	objectCeilingRocks->InitialiseMesh(mMeshCeilingRocks);
	objectCeilingRocks->SetMeshRotation(DirectX::XMFLOAT3(180.0f, 90.0f, 0.0f));
	objectCeilingRocks->SetMeshPosition(pos);
	mObjectCeilingRocks = gameState->AddGameObject(std::move(objectCeilingRocks));

	// cliff 1
	Mesh* mMeshCliff1 = new Mesh();
	mMeshDataCliff1 = new Mesh::MeshData();

	std::vector<Mesh::InstanceType> meshCliff1Instances;
	meshCliff1Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 500.0f, 900.0f)));
	meshCliff1Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 500.0f, 1200.0f)));
	meshCliff1Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, -400.0f, 1800.0f)));
	meshCliff1Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, -400.0f, 2100.0f)));
	mMeshDataCliff1->instanceData = meshCliff1Instances;

	mMeshDataCliff1->modelName = "resources/models/cliff_wall";

	mMeshCliff1->Initialise(gameState->GetD3DManager()->GetDevice(), *mMeshDataCliff1);

	std::unique_ptr<GameObject> objectCliff1 = std::make_unique<GameObject>(gameState);
	objectCliff1->InitialiseMesh(mMeshCliff1);
	objectCliff1->SetMeshRotation(DirectX::XMFLOAT3(0.0f, 90.0f, 45.0f));
	objectCliff1->SetMeshPosition(pos);
	mObjectCliff1 = gameState->AddGameObject(std::move(objectCliff1));

	// cliff 2
	Mesh* mMeshCliff2 = new Mesh();
	mMeshDataCliff2 = new Mesh::MeshData();

	std::vector<Mesh::InstanceType> meshCliff2Instances;
	meshCliff2Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 500.0f, -900.0f)));
	meshCliff2Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 500.0f, -1200.0f)));
	meshCliff2Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, -400.0f, -1800.0f)));
	meshCliff2Instances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, -400.0f, -2100.0f)));
	mMeshDataCliff2->instanceData = meshCliff2Instances;

	mMeshDataCliff2->modelName = "resources/models/cliff_wall";

	mMeshCliff2->Initialise(gameState->GetD3DManager()->GetDevice(), *mMeshDataCliff2);

	std::unique_ptr<GameObject> objectCliff2 = std::make_unique<GameObject>(gameState);
	objectCliff2->InitialiseMesh(mMeshCliff2);
	objectCliff2->SetMeshRotation(DirectX::XMFLOAT3(0.0f, 270.0f, 45.0f));
	objectCliff2->SetMeshPosition(pos);
	mObjectCliff2 = gameState->AddGameObject(std::move(objectCliff2));

	// platform
	Mesh* mMeshPlatform = new Mesh();
	mMeshDataPlatform = new Mesh::MeshData();

	std::vector<Mesh::InstanceType> meshPlatformInstances;
	meshPlatformInstances.push_back(Mesh::InstanceType(DirectX::XMFLOAT3(0.0f, 10.0f, 0.0f)));
	mMeshDataPlatform->instanceData = meshPlatformInstances;

	mMeshDataPlatform->modelName = "resources/models/crane_platform";

	mMeshPlatform->Initialise(gameState->GetD3DManager()->GetDevice(), *mMeshDataPlatform);

	std::unique_ptr<GameObject> objectPlatform = std::make_unique<GameObject>(gameState);
	objectPlatform->InitialiseMesh(mMeshPlatform);
	objectPlatform->InitialiseCollisions(
		AddFloat3(pos, DirectX::XMFLOAT3(0.0f, -25.0f, 0.0f)),
		0.0f,
		new btBoxShape(btVector3(172.0f, 10.0f, 160.0f)));
	objectPlatform->SetMeshRotation(DirectX::XMFLOAT3(-90.0f, 0.0f, 0.0f));
	mObjectPlatform = gameState->AddGameObject(std::move(objectPlatform));

	// floor
	Mesh* meshFloor = new Mesh();
	Mesh::MeshData meshDataFloor;
	meshDataFloor.pos = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	std::vector<Mesh::VertexType> vertexDataFloor;
	float floorWidth = 3000.0f;
	float floorDepth = 500.0f;
	float floorHeight = -200.0f;
	float floorU = 5.0f;
	float floorV = 0.5f;

	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(-floorDepth, floorHeight, -floorWidth), DirectX::XMFLOAT2(0, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(-floorDepth, floorHeight, floorWidth), DirectX::XMFLOAT2(floorU, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, floorWidth), DirectX::XMFLOAT2(floorU, floorV)));

	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(-floorDepth, floorHeight, -floorWidth), DirectX::XMFLOAT2(0, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, floorWidth), DirectX::XMFLOAT2(floorU, floorV)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, -floorWidth), DirectX::XMFLOAT2(0, floorV)));

	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, -floorWidth), DirectX::XMFLOAT2(0, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, floorWidth), DirectX::XMFLOAT2(floorU, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(floorDepth, floorHeight, floorWidth), DirectX::XMFLOAT2(floorU, floorV)));

	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(0.0f, 0.0f, -floorWidth), DirectX::XMFLOAT2(0, 0)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(floorDepth, floorHeight, floorWidth), DirectX::XMFLOAT2(floorU, floorV)));
	vertexDataFloor.push_back(Mesh::VertexType(DirectX::XMFLOAT3(floorDepth, floorHeight, -floorWidth), DirectX::XMFLOAT2(0, floorV)));

	meshDataFloor.vertexData = vertexDataFloor;
	meshDataFloor.overrideTextureName = "resources/textures/lava.png";

	meshFloor->Initialise(gameState->GetD3DManager()->GetDevice(), meshDataFloor);

	std::unique_ptr<GameObject> objectFloor = std::make_unique<GameObject>(gameState);
	objectFloor->InitialiseMesh(meshFloor);	
	objectFloor->SetMeshPosition(AddFloat3(pos, DirectX::XMFLOAT3(0.0f, -1000.0f, 0.0f)));
	gameState->AddGameObject(std::move(objectFloor));
}

void HangingPlatform::Shutdown()
{
	mGameState->RemoveGameObject(mObjectChains);
	mObjectChains = nullptr;

	mGameState->RemoveGameObject(mObjectCeilingRocks);
	mObjectCeilingRocks = nullptr;

	mGameState->RemoveGameObject(mObjectPlatform);
	mObjectPlatform = nullptr;

	mGameState->RemoveGameObject(mObjectCliff1);
	mObjectCliff1 = nullptr;

	mGameState->RemoveGameObject(mObjectCliff2);
	mObjectCliff2 = nullptr;
}
