#include "Player.h"

#include <Engine\GameStates\GameState.h>
#include <Engine\Core\InputManager.h>
#include <Engine\Graphics\Core\Camera.h>
#include <Engine\Utilities\MathUtilities.h>

Player::Player(GameStates::GameState* gameState, D3DManager* d3dManager, Camera* camera, DirectX::XMFLOAT3 startPos) :
	GameObject(gameState)
{
	mCamera = camera;
	mMidAir = true;
	mRunning = false;

	// collisions
	GameObject::InitialiseCollisions(
		startPos,
		10.0f,
		new btBoxShape(btVector3(15.0f, 40.0f, 15.0f)));

	mBody->setAngularFactor(btVector3(0.0f, 0.0f, 0.0f));
	mBody->setFriction(0.0f);

	mMoveSpeed = INITIAL_MOVE_SPEED;
	mJumpForce = INITIAL_JUMP_FORCE;
}

void Player::Collision(GameObject* collider)
{
	CollisionCallback callback = CollisionCallback(*mBody.get());
	mGameState->GetPhysicsManager()->GetWorld()->contactTest(mBody.get(), callback);

	if (mMidAir)
	{
		if (callback.lowestAngle < 45.0f)
		{
			mMidAir = false;
		}
	}
}

void Player::Update()
{
	GameObject::Update();

	// check when to start running
	if (InputManager::IsKeyPressed('W'))
	{
		mRunning = true;
	}

	// if running
	if(mRunning)
	{
		// move player forward at view angle
		DirectX::XMFLOAT3 cameraForward = mCamera->GetForward();
		cameraForward.y = 0.0f;

		DirectX::XMStoreFloat3(&cameraForward, DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&cameraForward)));

		cameraForward = MultiplyFloat3(cameraForward, mMoveSpeed);

		// get old body velocity
		btVector3 oldVelocity = mBody->getLinearVelocity();

		btVector3 bodyVelocity(cameraForward.x, oldVelocity.getY(), cameraForward.z);

		mBody->setLinearVelocity(bodyVelocity);

		// jump
		if (InputManager::IsKeyPressed(VK_SPACE))
		{
			if (!mMidAir)
			{
				btVector3 velocity = mBody->getLinearVelocity();
				velocity.setY(velocity.getY() + mJumpForce);

				mBody->setLinearVelocity(velocity);

				mMidAir = true;
			}
		}

		// increment move speed
		mMoveSpeed += MOVE_SPEED_INCREMENT;
	}
}
