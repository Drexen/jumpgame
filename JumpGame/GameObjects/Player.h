#pragma once

#include <Engine\GameObjects\GameObject.h>
#include <Engine\Graphics\Core\D3DManager.h>

class Camera;

namespace GameStates
{
	class GameState;
}

class Player : public GameObject
{
public:

	struct CollisionCallback : public btCollisionWorld::ContactResultCallback
	{
		CollisionCallback(btRigidBody& body) :
			btCollisionWorld::ContactResultCallback(),
			mBody(body),
			lowestAngle(90.0f)
		{

		}

		btRigidBody &mBody;	
		float lowestAngle;

		// called with each contact
		virtual btScalar addSingleResult(
			btManifoldPoint& manifold,
			const btCollisionObjectWrapper* collisionObject0, int partId0, int index0,
			const btCollisionObjectWrapper* collisionObject1, int partId1, int index1)
		{
			btVector3 contactPoint;

			if (collisionObject0->m_collisionObject == &mBody)
			{
				contactPoint = manifold.m_localPointA;
			}
			else
			{
				// body does not match either collision object
				assert(collisionObject1->m_collisionObject == &mBody);

				contactPoint = manifold.m_localPointB;
			}

			float angleRads = btVector3(0.0f, 1.0f, 0.0f).angle(manifold.m_normalWorldOnB);

			float angle = DirectX::XMConvertToDegrees(angleRads);

			if (angle < lowestAngle)
			{
				lowestAngle = angle;
			}
			
			return 0;
		}
	};

public:
	const float INITIAL_MOVE_SPEED = 400.0f;
	const float INITIAL_JUMP_FORCE = 500.0f;

	const float MOVE_SPEED_INCREMENT = 0.05f;

public:

	Player(GameStates::GameState* gameState, D3DManager* d3dManager, Camera* camera, DirectX::XMFLOAT3 startPos);

	void Collision(GameObject* collider);

	void Update();

private:

	float mMoveSpeed;
	float mJumpForce;

	Camera* mCamera;

	bool mMidAir;
	bool mRunning;
};
