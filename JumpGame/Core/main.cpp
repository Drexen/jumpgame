#include <windows.h>
#include <memory>

#include <Engine\Core\Application.h>
#include <JumpGame\GameStates\MainMenu.h>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	std::unique_ptr<Application> application = std::make_unique<Application>();
	if (!application.get())
	{
		return 0;
	}

	// setup initial GameState
	std::unique_ptr<GameStates::MainMenu> initialGameState;
	initialGameState = std::make_unique<GameStates::MainMenu>(application.get());

	if (application->Initialise(std::move(initialGameState)))
	{		
		// run the application
		application->Run();
	}

	application->Shutdown();

	return 0;
}
